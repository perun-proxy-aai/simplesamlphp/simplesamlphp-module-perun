# perun-simplesamlphp-module

![maintenance status: end of life](https://img.shields.io/maintenance/end%20of%20life/2024)

This project has reached end of life, which means no new features will be added. Security patches and important bug fixes will end as of 2024. Check out [Apereo CAS](https://apereo.github.io/cas/) instead.

## Description

Module which allows simpleSAMLphp to get data from Perun. Contains theme for LS AAI, LS Hostel functionality and SP
to check released attributes (attribute_check).

## Contribution

This repository uses [Conventional Commits](https://www.npmjs.com/package/@commitlint/config-conventional).

Any change that significantly changes behavior in a backward-incompatible way or requires a configuration change must be marked as BREAKING CHANGE.

### Available scopes:

- theme
- lshostel
- attribute_check
- Auth Process filters:
  - ensurevomember
  - forceaup
  - idpattribute
  - logininfo
  - perunattributes
  - entitlement
  - perunidentity
  - targetedid
  - proxyfilter
  - removeallattributes
  - idpentityid
  - stringifytargetedid
  - updateues
  - warningtestsp
- ...

## Instalation

Once you have installed SimpleSAMLphp, installing this module is very simple. First, you will need to download Composer if you haven't already. After installing Composer, just execute the following command in the root of your SimpleSAMLphp installation:

`php composer.phar require cesnet/simplesamlphp-module-perun`
