## ProxyFilter

This filter allows to disable/enable nested filters for particular SP or for users with one of denied/allowed attribute values.

```php
24 => [
    'class' => 'perun:ProxyFilter',
    //'mode' => 'allowlist', // defaults to 'denylist'
    'filterSPs' => ['entityID1', 'entityID2'], // list of entityIDs
    'filterAttributes' => ['attrName1'=>['value1','value2'], 'attrName2'=>['value3','value4']], // user attributes in the format attrName => values_list
    'authproc' => [
        [/* first filter */],
        [/* second filter */],
        /* etc. */
    ],
],
```

## PerunIdentity

Example how to configure PerunIdentity module:

```php
24 => [
        'class' => 'perun:ProxyFilter',
        'filterSPs' => $perunEntityIds,
        'config' => [
                'class' => 'perun:PerunIdentity',
                'uidsAttr' => ['eduPersonUniqueId', 'eduPersonPrincipalName', 'eduPersonTargetedIDString', 'nameid', 'uid'],
                'voShortName' => 'einfra',
                'registerUrlBase' => 'https://perun.cesnet.cz/allfed/registrar',
                'registerUrl' => 'https://login.cesnet.cz/register',
                'interface' => 'ldap',
                'useAdditionalIdentifiersLookup' => true,
                'additionalIdentifiersAttribute' => 'additionalIdentifiers',
                'facilityCheckGroupMembershipAttr' => 'urn:perun:facility:attribute-def:def:checkGroupMembership',
                'facilityVoShortNamesAttr' => 'urn:perun:facility:attribute-def:virt:voShortNames',
                'facilityDynamicRegistrationAttr' => 'urn:perun:facility:attribute-def:def:dynamicRegistration',
                'facilityRegisterUrlAttr' => 'urn:perun:facility:attribute-def:def:registerUrl',
                'facilityAllowRegistrationToGroups' => 'urn:perun:facility:attribute-def:def:allowRegistration',
                #MODE:
                # * FULL - Get user from Perun and check if user has correct rights to access service
                # * USERONLY - Only get user from Perun
                'mode' => 'FULL' #Default value: FULL
                #'accessControlDisabledAttr' => 'perunResourceAttribute_proxyAccessControlDisabled',
        ],
],
```

## IdPAttribute

Example how to enable filter IdPAttribute:

```php
29 => [
        'class' => 'perun:IdPAttribute',
        'attrMap' => [
                'OrganizationName:en' => 'idp_organizationName',
        ],
],
```

'OrganizationName:en' => 'idp_organizationName' means that the $IdPMetadata['Organization']['en'] will be save into 
$request['Attributes']['idp_organizationname']

## EnsureVoMember

Example how to configure filter EnsureVoMember:

```php
31 => array(
    'class' => 'perun:EnsureVoMember',
    'triggerAttr' => 'triggerAttr',
    'voDefsAttr' => 'voDefsAttr',
    'registrarURL' => 'https://www.registrarUrl.com',
    'interface' => 'ldap',
    'loginUrlAttr' => 'facilityLoginAttr', # Will be used as callback if specified. If not callback will be back to proxy
),
```

## UpdateUserExtSource

Example how to configure UpdateUserExtSource:

```php
32 => array(
    'class' => 'perun:UpdateUserExtSource',
    # 'eduPersonEntitlement is the preconfigured default option
    'eduPersonEntitlement' => 'eduPersonEntitlement'
),
```
Updates `userExtSource` attributes when the user logs in. Saves only the entitlements issued by an authority other than the current proxy for security reasons.

## PerunEntitlement

Example how to enable/configure filter PerunEntitlement:

```php
33 => array(
    'class' => 'perun:PerunEntitlement',
    'interface' => 'ldap',
    'eduPersonEntitlement' => 'eduPersonEntitlement',
    # forwarded entitlement are released by default
    #'releaseForwardedEntitlement' => false, OPTIONAL
    'forwardedEduPersonEntitlement' => 'eduPersonEntitlement',
    #'entityID' => function($request){return empty($request["saml:RequesterID"]) ? $request["SPMetadata"]["entityid"] : $request["saml:RequesterID"][0];},
    #'groupEntitlementDisabledAttr' => 'perunResourceAttribute_groupEntitlementDisabled',
),
```

Retrieves only the entitlements issued by an authority other than the current proxy for security reasons.
## PerunEntitlementExtended

Example how to enable/configure filter PerunEntitlement:

```php
33 => array(
    'class' => 'perun:PerunEntitlementExtended',
    'interface' => 'ldap',
    'outputAttrName' => 'eduPersonEntitlementExtended',
    # forwarded entitlement are released by default
    #'releaseForwardedEntitlement' => false, OPTIONAL
    'forwardedEduPersonEntitlement' => 'eduPersonEntitlement',
    #'entityID' => function($request){return empty($request["saml:RequesterID"]) ? $request["SPMetadata"]["entityid"] : $request["saml:RequesterID"][0];},
    #'groupEntitlementDisabledAttr' => 'perunResourceAttribute_groupEntitlementDisabled',
),
```

Retrieves only the entitlements issued by an authority other than the current proxy for security reasons.
## ForceAup

1.Create these attributes in Perun:

- urn:perun:entityless:attribute-def:def:orgAups
  - Type: LinkedHashMap
  - Unique: no
  - Read:
  - Write:
- urn:perun:user:attribute-def:def:aups

  - Type: LinkedHashMap
  - Unique: no
  - Read: SELF, FACILITY, VO
  - Write:

- urn:perun:vo:attribute-def:def:aup
  - Type: LargeString
  - Unique: no
  - Read: VO
  - Write: VO
- urn:perun:facility:attribute-def:def:reqAups
  - Type: ArrayList
  - Unique: no
  - Read: FACILITY
  - Write: FACILITY
- urn:perun:facility:attribute-def:virt:voShortNames - Type: ArrayList - Unique: no - Read: FACILITY - Write: FACILITY
  2.Configure SimpleSAMLphp to use ForceAup:

Example how to enable filter ForceAup:

```php
40 => [
    'class' => 'perun:ProxyFilter',
    'filterSPs' => $perunEntityIds,
    'config' => [
        'class' => 'perun:ForceAup',
        'interface' => 'rpc',
        'perunAupsAttr' => 'urn:perun:entityless:attribute-def:def:orgAups',
        'perunUserAupAttr' => 'urn:perun:user:attribute-def:def:aups',
        'perunVoAupAttr' => 'urn:perun:vo:attribute-def:def:aup',
        'perunFacilityReqAupsAttr' => 'urn:perun:facility:attribute-def:def:reqAups',
        'perunFacilityVoShortNamesAttr' => 'urn:perun:facility:attribute-def:virt:voShortNames'
    ],
],
```

3.Fill the attributes and set list of required Aups (attr reqAups) and voShortNames (optional) for each facility

## AttributeMap

This filter maps attribute names according to a service specific map from Perun. It can be used to achieve compatibility with a SP which requires specific non-standard attribute names.

Example how to enable filter AttributeMap:

```php
101 => [
    'class' => 'perun:AttributeMap',
    'attrMapAttr' => 'attrWhichContainsAttrMap', # expected structure: targetAttribute => sourceAttribute
    // 'keepSourceAttributes' => true, # optional, whether keep source attributes or remove them, default false
    // 'entityid' => 'EntityIdOfTheService', # optional, string or callable, defaults to current SP's entity ID
    // 'interface' => 'rpc', # optional, rpc/ldap, default rpc
],
```

## ExtractRequestAttribute

Filter is intended to extract an attribute specified by set of keys forming the chain of keys in the `$request` variable into the configured destination attribute.

Configuration options:

- `destination_attr_name`: specifies attribute name, into which the extracted value will be stored
- `request_keys`: string, which contains a semicolon (`;`) separated chain of keys that are examined in the state. Numeric keys are automatically treated as array indexes. For instance, value `'saml:AuthenticatingAuthority;0'` will be treated as code `$request['saml:AuthenticatingAuthority'][0]`. In case of this value being empty, exception is thrown. Otherwise, extracted value is stored into the configured destination attribute.
- `fail_on_nonexisting_keys`: `true` or `false`, specifies if in case of missing key in the request variable the filter should terminate with an exception or not
- `default_value`: array, which will be set as default value, if the configured keys did not lead to value

```php
// EXTRACT AUTHENTICATING ENTITY INTO authenticating_idp attribute
1 => [
    'class' => 'perun:ExtractRequestAttribute',
    'destination_attr_name' => 'authenticating_idp',
    'request_keys' => 'saml:AuthenticatingAuthority;0',
    'fail_on_nonexisting_keys' => 'true',
    'default_value' => [],
],
```

## PerunUser

Filter tries to identify the Perun user. It uses the combination of user identifier and IdP identifier to find the user (or to be more precise, the user identity and associated user account). If it can, the user object is set to `$request` parameter into `$request[PerunConstants::PERUN][PerunConstants::USER]`. Otherwise, user is forwarded to configured registration.

Configuration options:

- `interface`: specifies what interface of Perun should be used to fetch data. See class `SimpleSAML\Module\perun\PerunAdapter` for more details.
- `uid_attrs`: list of attributes that contain user identifiers to be used for identification. The order of the items in the list represents the priority.
- `idp_id_attr`: name of the attribute (from `$request['Attributes']` array), which holds EntityID of the identity provider that has performed the authentication.
- `register_url`: URL to which the user will be forwarded for registration. Leave empty to use the Perun registrar.
- `callback_parameter_name`: name of the parameter wich will hold callback URL, where the user should be redirected after the registration on URL configured in the `register_url` property.
- `perun_register_url`: the complete URL (including vo and group) to which user will be redirected, if `register_url` has not been configured. Parameters targetnew, targetexisting and targetextended will be set to callback URL to continue after the registration is completed.
- `use_additional_identifiers_lookup`: `true` or `false`, set it to `true` if you want to use additionalIdentifiers as fallback lookup method if the standard one fails.
- `additional_identifiers_attribute`: name of the attribute (from `$request['Attributes']` array), which holds the additional identifiers. If you use RPC adapter, the value in the attribute resolved using `idp_id_attr` will be used as well for locating the user in Perun.
- `use_regular_ids_in_auids`: if set to `true`, will enable usage of `uid_attrs` values in AUIDs lookup (e.g. raw EPPN value, EPTID value etc.) 

```php
2 => [
    'class' => 'perun:PerunUser',
    'interface' => 'LDAP',
    'uid_attrs' => ['eduPersonUniqueId', 'eduPersonPrincipalName'],
    'idp_id_attr' => 'authenticating_idp',
    'register_url' => 'https://signup.cesnet.cz/',
    'callback_parameter_name' => 'callback',
    'perun_register_url' => 'https://signup.perun.cesnet.cz/fed/registrar/?vo=cesnet',
    'use_additional_identifiers_lookup' => true,
    'additional_identifiers_attribute' => 'additionalIdentifiers',
    'user_regular_ids_in_auids' => true,
],
```

## PerunAup

Filter fetches the given attribute holding approved AUP and checks, if expected value is set in the attribute or not. If not, it redirects the user to specified registration component, where user will be asked to approve the AUP.

Configuration options:

- `interface`: specifies what interface of Perun should be used to fetch data. See class `SimpleSAML\Module\perun\PerunAdapter` for more details.
- `attribute`: name of the attribute, which will be fetched from Perun and holds the value of approved AUP.
- `value`: value that is expected in the attribute as mark of approved AUP. Expected is a string.
- `approval_url`: URL to which the user will be forwarded for registration. Leave empty to use the Perun registrar.
- `callback_parameter_name`: name of the parameter wich will hold callback URL, where the user should be redirected after the AUP approval on URL configured in the `approval_url` property.
- `perun_register_url`: the complete URL (including vo and group) to which user will be redirected, if `approval_url` has not been configured. Parameters targetnew, targetexisting and targetextended will be set to callback URL to continue after the AUP approval is completed.

```php
3 => [
    'class' => 'perun:PerunAup',
    'interface' => 'LDAP',
    'value' => 'aup_2020_01_01',
    'attribute' => 'approved_aup',
    'approval_url' => 'https://signup.cesnet.cz/aup/',
    'callback_parameter_name' => 'callback',
    'perun_approval_url' => 'https://signup.perun.cesnet.cz/fed/registrar/?vo=cesnet&group=aup'
],
```

## DropUserAttributes

Drops specified user attributes from the `$request['Attributes']` variable.

Configuration options:

- `attribute_names`: list of attribute names which will be dropped.

```php
10 => [
    'class' => 'perun:DropUserAttributes',
    'attribute_names' => ['aup', 'eppn', 'eduPersonTargetedID']
],
```

## QualifyNameID

Adds qualifiers into NameID based on the configuration

Configuration options:

- `name_id_attribute`: Attribute (NameID) which should be qualified
- `name_qualifier_attribute`: User attribute with value, which will be set as the NameQualifier part of the NameID. Leave empty to use static value configured via option `name_qualifier`.
- `name_qualifier`: Static value which will be set as the NameQualifier part of the NameID.
- `sp_name_qualifier_attribute`: User attribute with value, which will be set as the SPNameQualifier part of the NameID. Leave empty to use static value configured via option `sp_name_qualifier`.
- `sp_name_qualifier`: Static value which will be set as the SPNameQualifier part of the NameID.

```php
11 => [
    'class' => 'perun:QualifyNameID',
    'name_id_attribute' => 'eduPersonTargetedID',
    'name_qualifier_attribute' => 'SourceIdPEntityID',
    'name_qualifier' => 'https://login.cesnet.cz/idp/',
    'sp_name_qualifier_attribute' => 'ProxyEntityID',
    'sp_name_qualifier' => 'https://login.cesnet.cz/proxy/',
],
```

## GenerateIdPAttributes

Gets metadata of the IdP specified by `idp_identifier_attribute` value and tries to set the specified keys from IdP metadata into attributes.

Configuration options:

- `idp_identifier_attribute`: Attribute holding the identifier of the Authenticating IdP
- `attribute_map`: Map of IdP metadata attributes, where keys are the colon separated keys that will be searched in IdP metadata and values are the destination attribute names.

```php
20 => [
    'class' => 'perun:GenerateIdPAttributes',
    'idp_identifier_attribute' => 'sourceIdPEntityID',
    'attribute_map' => [
        'name:en' => 'sourceIdPName',
        'OrganizationName:en' => 'sourceIdPOrganizationName',
        'OrganizationURL:en' => 'sourceIdPOrganizationURL',
    ],
],
```

## SpAuthorization

Performs authorization check define dby the SP based on group membership in Perun. User has to be valid member of at least one of the groups assigned to resources of the facility representing the service. If not satisfied, the filter check if registration is enabled. In case of enabled registration, user is forwarded to custom registration link (if configured), or to a dynamic form, where user will select the combination of VO and group to which he/she applies for access. Form then forwards user to Perun registration component. In all other cases, user is forwarded to access denied page.
NOTE: for correct functionality, RPC adapter must be available, as other adapters cannot fetch info about what groups allow registration (have registration forms) and similar data.

Configuration options:

- `interface`: specifies what interface of Perun should be used to fetch data. See class `SimpleSAML\Module\perun\PerunAdapter` for more details.
- `registrar_url`: URL where Perun registration component is located. Expected URL is the base, without any parameters.
- `check_group_membership_attr`: mapping to the attribute containing flag, if membership check should be performed.
- `vo_short_names_attr`: mapping to the attribute containing shortnames of the VOs for which the service has resources (gives access to the groups).
- `registration_link_attr`: mapping to the attribute containing custom service registration link. Filter adds the callback URL, to which to redirect user after the registration, as query string in form of 'callback=URL'.
- `allow_registration_attr`: mapping to the attribute containing flag, if registration in case of denied access is enabled
- `handle_unsatisfied_membership`: whether handle unsatisfied membership

```php
25 => [
    'class' => 'perun:SpAuthorization',
    'interface' => 'ldap',
    'registrar_url' => 'https://signup.perun.cesnet.cz/fed/registrar/',
    'check_group_membership_attr' => 'check_group_membership',
    'vo_short_names_attr' => 'vo_short_names',
    'registration_link_attr' => 'registration_link',
    'allow_registration_attr' => 'allow_registration',
    'handle_unsatisfied_membership' => true,
    #'accessControlDisabledAttr' => 'perunResourceAttribute_proxyAccessControlDisabled',
],
```

## EnsureVOMember

Checks whether the user is in the given VO (group). If not, redirects him/her to the registration.

Configuration options:

- `registrationUrl`: URL to the registration
- `voShortName`: VO shortname to check the user's membership
- `groupName`: OPTIONAL, checks that user is in given group
- `callbackParameterName`: name of the parameter wich will hold callback URL, where the user should be redirected after the AUP approval on URL configured in the `approval_url` property,
- `interface`: specifies what interface of Perun should be used to fetch data. See class `SimpleSAML\Module\perun\PerunAdapter` for more details.

```php
25 => [
    'class' => 'perun:PerunEnsureMember',
    'registerUrl' => 'https://signup.perun.cesnet.cz/fed/registrar/',
    'voShortName' => 'cesnet',
    'groupName' => 'cesnet_group_name', // optional
    'callbackParameterName' => 'targetnew',
    'interface' => 'ldap',
],
```

## EnsureVoAup

Filter checks if user accepted AUPs for VO specified by short name in attribute (Has to be enabled by trigger attribute). By given VO configuration it checks if expected value is set in the user attribute or not. If not, it redirects the user to specified registration component, where user will be asked to approve the AUP.

Filter should be run after EnsureVoMember filter.

Configuration options:

- `interface`: specifies what interface of Perun should be used to fetch data. See class `SimpleSAML\Module\perun\PerunAdapter` for more details.
- `perun_approval_url`: the perun registrar url to which user will be redirected. The VO and Group parameters will be added dynamically. Parameters targetnew, targetexisting and targetextended will be set to callback URL to continue after the AUP approval is completed.
- `trigger_attr`: attribute which specify if the filter should be executed (boolean)
- `vo_defs_attr`: attribute with VO short name to proc AUP
- `vo_aup_conf`: array with VO short names as key and configs for the vo as values
- `vo_aup_conf.perun_approval_group`: group representing the AUP
- `vo_aup_conf.aup_value`: the value representing which AUP was accepted
- `vo_aup_conf.user_attribute`: the value to store the AUP acceptance

```php
3 => [
    'class' => 'perun:PerunAup',
    'interface' => 'ldap',
    'perun_approval_url' => 'https://signup.perun.cesnet.cz/fed/registrar/',
    'trigger_attr' => 'urn:perun:facility:attribute-def:def:rpEnableEnsureVoFiltering',
    'vo_defs_attr' => 'urn:perun:facility:attribute-def:def:rpEnsureVoDefinition',
    'vo_aup_conf' => [
        'bbmri' => [
            'perun_approval_group' => 'aup',
            'aup_value' => 'aup_1_3',
            'user_attribute' => 'urn:perun:user:attribute-def:def:userAup:bbmri'
        ],
        'elixir' => [
            'perun_approval_group' => 'aup',
            'aup_value' => 'aup_2016_06_14',
            'user_attribute' => 'urn:perun:user:attribute-def:def:userAup:elixir'
        ],
    ],
],
```


## IsEligible

Checks the eligibility timestamp. If the value is not older than `validity_period_months`, it lets the user in. Otherwise, user is forced to reauthenticate using other identity.

Configuration options:

- `trigger_attribute`: attribute name which has to be consumed by the service (in the SP remote metadata part attributes) to run this filter. If the attribute specified by the option is not released, filter is skipped.
- `eligible_last_seen_timestamp_attribute`: attribute containing the timestamp of last eligible login (timestamp of this event). Has to be available in `$state[PerunConstants::Attributes]`.
- `validity_period_months`: if the timestamp is older that this number of months, user won't be let in.
- `translations`: inline translations for the unauthorized page.

```php
25 => [
    'class' => 'perun:IsEligible',
    'trigger_attribute' => 'isCesnetEligible',
    'eligible_last_seen_timestamp_attribute' => 'isCesnetEligibleLastSeen',
    'validity_period_months' => 12,
    'translations' => [
      'old_value_header'=> [
        'en' => '...',
        'cs' => '...',
      ],
      'old_value_text'=> [
        'en' => '...',
        'cs' => '...',
      ],
      'old_value_button'=> [
        'en' => '...',
        'cs' => '...',
      ],
      'old_value_contact'=> [
        'en' => '...',
        'cs' => '...',
      ],
      'no_value_header'=> [
        'en' => '...',
        'cs' => '...',
      ],
      'no_value_text'=> [
        'en' => '...',
        'cs' => '...',
      ],
      'no_value_button'=> [
        'en' => '...',
        'cs' => '...',
      ],
      'no_value_contact'=> [
        'en' => '...',
        'cs' => '...',
      ],
    ]
],
```

## LoginInfo

Log a message about which user logged in using which external identity (IdP).

Configuration options:

- `attrs`: list of attribute names treated as identifiers
- `sourceIdPAttr`: name of an attribute which contains upstream IdP entity ID
- `sourceIdentifierAttr`: name of an attribute which contains upstream user identifier

```php
50 => [
    'class' => 'perun:LoginInfo',
    'attrs' => [
        'eduPersonUniqueId',
        'eduPersonPrincipalName',
    ],
    'sourceIdPAttr' => 'sourceIdPEntityID',
    'sourceIdentifierAttr' => 'sourceIdPEppn',
],
```

## IsBanned

Checks in a database storing user bans, if a ban for the user exists. If so, redirects user
to configured URL.

Configuration options:

- `redirectUrl`: URL to redirect banned users to
- `table_name`: Name of table in SQL database
- `column_name`: Name of columd to search userID in

```php
51 => [
    'class': 'perun:IsBanned',
    'redirect_url' => 'https://example.org/banned.html',
    'table_name' => 'table_name',
    'column_name' => 'column_name',
]
```

## AddAttributeValueBasedOnVoMember

Adds values based on the valid membership in a VO with specified ID. Configuration requires an associative array
where key is the ID of a VO and the value is an array of values to be added when the user is an active member in the
respective VO.

Example how to configure filter AddAttributeValueBasedOnVoMember:

```php
31 => array(
  'class' => 'perun:AddAttributeValuesBasedOnVoMember',
  'membership_map' => [
      3345 => ['affiliate@lifescience-ri.eu'],
      3322 => ['affiliate@elixir-europe.org'],
      3353 => ['affiliate@bbmri.eu'],
  ],
  'interface' => 'ldap',
  'destination_attribute' => 'eduPersonScopedAffiliation'
),
```

## AdditionalIdentifiers

Generates hashed identifiers from the original source attributes. Values matched with RegExes are concatenated in the 
order as specified in the config, concatenated with the salt and hashed. All values are then assembled into a list and
stored in the specified attribute.

Configuration options:

- `hashSalt`: salt string used for hashing
- `hashFunction`: hashing function to be applied
- `hashAttributes`: array of associative arrays. Keys in the associative array are names of the attributes that should
be matched, values are regular expressions used for matching.

Example how to configure filter AdditionalIdentifiers:

```php
31 => array(
  'class' => 'perun:AdditionalIdentifiers',
  'config' => [
    'hashSalt' => 'exampleSalt',
    'hashFunction' => 'sha1',

    # hashAttributes is array of arrays, each item represents a unique attribute combination,
    # key represents attribute name and value is a regular expression matching value
    # each sublist represents one hash, if attribute does not exist or any value does not match RE,
    # the hash will not be generated
    # (for matching from start to end use ^ and $)
    'hashAttributes' => [
        [
            'attr1' => '/^val1$/'
        ],
        [
            'attr2' => '/val2/'
        ],
        [
            'attr3' => '/val3/',
            'attr4' => '/val4/',
        ],
        [
            'attr5' => '/.*/'
        ],
    ],
    'additionalIdentifiersAttribute' => 'additionalIdentifiersAttribute'
  ]
];

```

## AuthEventLegging

Get data about user and his authorization. Typical data like datetime, ip, user agent, location,
SP  and IdP name and identifier and requested and upstream ACRs

## PerunAffiliation

Generates eduPersonAffiliation and eduPersonPrimaryAffiliation from
a facility which represents the ProxyIdP (its resources represent each affiliation value).

Minimal config:

```php
33 => [
    'class' => 'perun:PerunAffiliation',
    'perunProxyIdPEntityID' => 'https://example.org/idp/shibboleth',
],
```

where `https://example.org/idp/shibboleth` is set as RP identifier (entity ID) on the facility.

All options and default values:

```php
33 => [
    'class' => 'perun:PerunAffiliation',
    'perunProxyIdPEntityID' => 'https://example.org/idp/shibboleth', // REQUIRED
    'interface' => 'rpc',
    'eduPersonAffiliationAttribute' => 'eduPersonAffiliation',
    'eduPersonPrimaryAffiliationAttribute' => 'eduPersonPrimaryAffiliation',
    'affiliationCapabilityPrefix' => 'res:proxyidp:affiliation:',
    'permanentEmployeeCapability' => 'res:proxyidp:employee-permanent',
    'fallbackAffiliation' => 'affiliate',
],
```

You need to setup a resource for each eduPersonAffiliation value,
and on each of these resources set Resource capability to `res:proxyidp:affiliation:<value>`,
e.g. `res:proxyidp:affiliation:student`. All users assigned through groups to this resource
get the affiliation `student`.

In order to accurately generate the primary affiliation, you also need to setup a resource
for permanent employees (those whose primary affiliation is `employee`). On this resource,
set Resource capability to `res:proxyidp:employee-permanent`.

If a user gets no affiliation (is not assigned to any relevant resource), the `fallbackAffiliation` is used.

## AUPManager

Redirect user to AUP manager application. If there are relevant AUPs user can accept them and will be redirected back
to `aup_manager_callback.php` where the result will be checked.

```php
34 => [
    'config' => [
                'class' => 'perun:AUPManager',
                'interface' => 'ldap',
                'redirect_url' => 'https://example.com/aup-manager/accept_aups',
                'result_check_url' => 'https://example.com/aup-manager/get_accept_result',
                'signing_key' => '{
                    .
                    .
                    .
                 }',
                'signing_alg' => 'RS256',
            ],
        ],
],
```