<?php

declare(strict_types=1);

use SimpleSAML\Module\perun\Auth\Process\PerunCreateMember;

$this->includeAtTemplateBase('includes/header.php');
?>

  <div class="row">
    <div>
        <?php if ($this->data[PerunCreateMember::PARAM_WAS_EXCEPTION]) { ?>
          <p><?php echo $this->t('{perun:perun:create_member_exception_text}'); ?></p>
        <?php } else { ?>
          <p><?php echo $this->t('{perun:perun:create_member_success_text}'); ?></p>
          <a class="btn btn-lg btn-block btn-primary"
             href="<?php echo $this->data[PerunCreateMember::PARAM_CALLBACK]; ?>">
              <?php echo $this->t('{perun:perun:create_member_button}'); ?>
          </a>
        <?php } ?>
    </div>
  </div>

<?php
$this->includeAtTemplateBase('includes/footer.php');
?>
