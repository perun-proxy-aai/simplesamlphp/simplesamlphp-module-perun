<?php

declare(strict_types=1);

use SimpleSAML\Module\perun\PerunConstants;

header('HTTP/1.0 403 Forbidden');

$this->data['header'] = '';

$spMetadata = $this->data[PerunConstants::SP_METADATA];
$serviceName = $this->t($spMetadata[PerunConstants::SP_NAME]);
$this->includeAtTemplateBase('includes/header.php');

?>
    <div class="error_message">
        <h1><?php echo $this->t('{perun:perun:aup_manager_403_header}'); ?></h1>
        <p>
            <?php
            echo $this->t('{perun:perun:aup_manager_403_text}') . '<b>' . $serviceName . '</b>. ' .
                $this->t('{perun:perun:aup_manager_403_reason}');
            ?>
        </p>
    </div>

<?php
$this->includeAtTemplateBase('includes/footer.php');
