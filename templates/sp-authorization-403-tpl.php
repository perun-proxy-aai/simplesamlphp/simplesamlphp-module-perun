<?php

declare(strict_types=1);

use SimpleSAML\Module\perun\Auth\Process\SpAuthorization;
use SimpleSAML\Module\perun\PerunConstants;

header('HTTP/1.0 403 Forbidden');

$this->data['header'] = '';

$spMetadata = $this->data[SpAuthorization::PARAM_SP_METADATA];
$serviceName = $this->t($spMetadata[PerunConstants::SP_NAME]);
$administrationContact = $spMetadata[PerunConstants::SP_ADMINISTRATION_CONTACT];
if (empty($administrationContact) && !empty($spMetadata[PerunConstants::SP_CONTACTS])) {
    $contacts = $spMetadata[PerunConstants::SP_CONTACTS];
    usort($contacts, function ($a, $b) use ($spMetadata) {
        switch ($a["contactType"]) {
            case "support":
                $p1 = 4;
                break;
            case "administrative":
                $p1 = 3;
                break;
            case "technical":
                $p1 = 2;
                break;
            default:
                $p1 = 1;
        }
        switch ($b["contactType"]) {
            case "support":
                $p2 = 4;
                break;
            case "administrative":
                $p2 = 3;
                break;
            case "technical":
                $p2 = 2;
                break;
            default:
                $p2 = 1;
        }
        if ($p1 !== $p2) {
            return $p2 - $p1;
        }
        return array_search($a, $spMetadata[PerunConstants::SP_CONTACTS])
          - array_search($b, $spMetadata[PerunConstants::SP_CONTACTS]);
    });
    $contact = array_values($contacts)[0]["emailAddress"] ?? null;
    if (is_array($contact)) {
        $contact = $contact[0];
    }
    if ($contact) {
        $administrationContact = $contact;
    }
}
$mailto = 'mailto:' . $administrationContact . '?subject=' . $this->t('{perun:perun:sp_authorize_403_subject}');
$informationUrl = empty($spMetadata[PerunConstants::SP_INFORMATION_URL]) ?
    '' : $this->t($spMetadata[PerunConstants::SP_INFORMATION_URL]);
$this->includeAtTemplateBase('includes/header.php');

?>
<div class="error_message">
    <h1><?php echo $this->t('{perun:perun:sp_authorize_403_header}'); ?></h1>
    <p>
        <?php
        echo $this->t('{perun:perun:sp_authorize_403_text}') . '<b>' . $serviceName . '</b>.';
        if (!empty($informationUrl)) {
            echo ' (' . $this->t('{perun:perun:sp_authorize_403_information_page}') .
                '<a target="_blank" href="' . $informationUrl . '">'
                . $this->t('{perun:perun:sp_authorize_403_information_page_link_text}') . '</a>.)';
        } ?>
    </p>
    <?php if (!empty($administrationContact)) : ?>
    <p>
        <?php echo $this->t('{perun:perun:sp_authorize_403_contact_support}'); ?>
        <a href="<?php echo $mailto; ?>"><?php echo $administrationContact; ?></a>.
    </p>
    <?php endif; ?>
</div>

<?php
$this->includeAtTemplateBase('includes/footer.php');
