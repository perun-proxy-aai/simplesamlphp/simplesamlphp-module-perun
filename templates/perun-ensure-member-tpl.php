<?php

declare(strict_types=1);

use SimpleSAML\Module\perun\Auth\Process\PerunEnsureMember;

$this->data['header'] = $this->t('{perun:perun:ensure_member_header}');

$this->includeAtTemplateBase('includes/header.php');

?>
<div class="row">
    <div class="col-xs-12">
        <p><?php echo $this->t('{perun:perun:ensure_member_text}'); ?></p>
        <form action="<?php echo htmlspecialchars($this->data[PerunEnsureMember::PARAM_REGISTRATION_URL]); ?>"
              method="GET">
            <?php foreach ($this->data[PerunEnsureMember::PARAM_PARAMS] as $key => $value) { ?>
                <input type="hidden" name="<?php echo htmlspecialchars($key); ?>"
                       value="<?php echo htmlspecialchars($value); ?>">
            <?php } ?>
            <input type="submit" class="btn btn-lg btn-block btn-primary"
                   value="<?php echo $this->t('{perun:perun:ensure_member_submit}'); ?>">
        </form>
    </div>
</div>
<?php

$this->includeAtTemplateBase('includes/footer.php');
