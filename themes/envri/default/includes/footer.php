<?php

use SimpleSAML\Module;

if (!empty($this->data['htmlinject']['htmlContentPost'])) {
    foreach ($this->data['htmlinject']['htmlContentPost'] as $c) {
        echo $c;
    }
}

?>

</div><!-- #content -->
</div><!-- #wrap -->

<div style="text-align: center;">
  <div id="footer" style="display: flex; justify-content: space-between; margin: 0 auto; max-width: 1000px;">

    <div>
      <img src="<?php echo Module::getModuleUrl('perun/res/img/envri_logo_120.png') ?>">
    </div>

    <div>
      <img src="<?php echo Module::getModuleUrl('perun/res/img/eu_logo_120.png') ?>">
    </div>

    <div>
      <p>Virtual ENVRI community platform is maintained thanks to ENVRI-FAIR project.
        The project received funding from the European Union’s Horizon 2020 research and innovation
        programme under grant agreement No 824068.</p>
      <p>ENVRI-FAIR is coordinated by Forschungszentrum Jülich.</p>
      <p><a href="mailto:elter@ics.muni.cz">elter@ics.muni.cz</a></p>
      <p>Copyright © ENVRI-FAIR <?php echo date("Y"); ?></p>
    </div>

  </div>
</div><!-- #footer -->

</body>
</html>
