<?php

declare(strict_types=1);

use SimpleSAML\Module;

if (!empty($this->data['htmlinject']['htmlContentPost'])) {
    foreach ($this->data['htmlinject']['htmlContentPost'] as $c) {
        echo $c;
    }
}
?>

</div>
</div>
</div>

<div class="container" id="footer">
    <div class="row mt-1">
        <div class="col-xs-3 col-md-2">
            <div class="img-wrap" id="footer-ls-logo">
                <a href="https://lifescience-ri.eu/ls-login/" target="_blank">
                    <img src="<?php echo Module::getModuleUrl('perun/res/img/lsaai_logo_120.png') ?>"
                         alt="European Life Science Research Infrastructures Logo">
                </a>
            </div>
        </div>
        <div class="col-xs-9 col-md-10 mt-xs-1">
            <p class="text-justify ">LS Login, an authentication service of the European Life Science Research
                Infrastructures (LS RI), is a community platform established via the EOSC-Life project and operated by
                Masaryk University, Brno, CZ. Visit our
                <a href="https://lifescience-ri.eu/ls-login/" target="_blank">homepage</a>
                or contact us at
                <a href="mailto:support@aai.lifescience-ri.eu">support@aai.lifescience-ri.eu</a>
                .</p>
        </div>
    </div>
    <div class="row mb-2 mt-1">
        <div class="col-xs-3 col-md-2">
            <div class="img-wrap" id="footer-eu-flag">
                <img src="<?php echo Module::getModuleUrl('perun/res/img/eu_logo_120.png') ?>"
                     alt="European Union flag">
            </div>
        </div>
        <div class="col-xs-9 col-md-10 mt-xs-1">
            <p class="text-justify">
                <a href="https://lifescience-ri.eu/" target="_blank">The European Life Science Research
                    Infrastructures</a> has received funding from the European Union’s Horizon 2020 research
                and innovation programme under grant agreement No 654248 and from the European Union’s Horizon
                2020 programme under grant agreement number 824087.
            </p>
        </div>
    </div>
</div>

</body>
</html>

