<?php

declare(strict_types=1);

use SimpleSAML\Auth\State;
use SimpleSAML\Configuration;
use SimpleSAML\Module;
use SimpleSAML\Module\perun\Auth\Process\EnsureVoMember;
use SimpleSAML\Module\perun\PerunConstants;
use SimpleSAML\XHTML\Template;

$config = Configuration::getInstance();
$t = new Template($config, EnsureVoMember::TEMPLATE);

$stateId = $_REQUEST[EnsureVoMember::PARAM_STATE_ID];
$state = State::loadState($stateId, EnsureVoMember::STAGE);

$data = $state[EnsureVoMember::STAGE];

if (array_key_exists(EnsureVoMember::PARAM_SERVICE_LOGIN_URL, $data)) {
    $callback = $data[EnsureVoMember::PARAM_SERVICE_LOGIN_URL];
} else {
    $callback = Module::getModuleURL(EnsureVoMember::CALLBACK, [
        EnsureVoMember::PARAM_STATE_ID => $stateId,
    ]);
}

$t->data[EnsureVoMember::PARAM_REGISTRATION_URL] = $data[EnsureVoMember::PARAM_REGISTRATION_URL];
$params = [
    PerunConstants::VO => $data[EnsureVoMember::PARAM_REGISTRATION_VO],
];

if (!empty($callback)) {
    $params[PerunConstants::TARGET_NEW] = $callback;
    $params[PerunConstants::TARGET_EXISTING] = $callback;
    $params[PerunConstants::TARGET_EXTENDED] = $callback;
}

$t->data[EnsureVoMember::PARAM_PARAMS] = $params;

$t->show();
