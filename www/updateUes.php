<?php

/**
 * Script for updating UES in separate thread.
 */

declare(strict_types=1);

use SimpleSAML\Logger;
use SimpleSAML\Module\perun\Adapter;
use SimpleSAML\Module\perun\ChallengeManager;
use SimpleSAML\Module\perun\UESUpdateHelper;

$adapter = Adapter::getInstance(Adapter::RPC);
$token = file_get_contents('php://input');

if (empty($token)) {
    http_response_code(400);
    exit('The entity body is empty');
}

$attributesFromIdP = null;
$attrMap = null;
$serializedAttributes = [];
$appendOnlyAttrs = [];
$perunUserId = null;
$id = null;
$sourceIdpAttribute = null;

try {
    $challengeManager = new ChallengeManager();
    $claims = $challengeManager->decodeToken($token);

    $attributesFromIdP = $claims[UESUpdateHelper::DATA][UESUpdateHelper::ATTRIBUTES];
    $attrMap = $claims[UESUpdateHelper::DATA][UESUpdateHelper::ATTR_MAP];
    $serializedAttributes = $claims[UESUpdateHelper::DATA][UESUpdateHelper::ATTR_TO_CONVERSION];
    $appendOnlyAttrs = $claims[UESUpdateHelper::DATA][UESUpdateHelper::APPEND_ONLY_ATTRS];
    $perunUserId = $claims[UESUpdateHelper::DATA][UESUpdateHelper::PERUN_USER_ID];
    $id = $claims[UESUpdateHelper::ID];
} catch (Exception $ex) {
    Logger::error(UESUpdateHelper::DEBUG_PREFIX . 'The token verification ended with an error.');
    http_response_code(400);
    exit;
}

$config = UESUpdateHelper::getConfiguration();

$sourceIdpAttribute = $config[UESUpdateHelper::SOURCE_IDP_ATTRIBUTE_KEY];
$identifierAttributes = $config[UESUpdateHelper::USER_IDENTIFIERS];

try {
    if (empty($attributesFromIdP[$sourceIdpAttribute][0])) {
        throw new Exception(
            sprintf(
                "%sInvalid attributes from IdP - Attribute '%s' is empty",
                UESUpdateHelper::DEBUG_PREFIX,
                $sourceIdpAttribute
            )
        );
    }

    $extSourceName = $attributesFromIdP[$sourceIdpAttribute][0];
    Logger::debug(sprintf("%sExtracted extSourceName: '%s'", UESUpdateHelper::DEBUG_PREFIX, $extSourceName));

    $userExtSource =
        UESUpdateHelper::findUserExtSource($adapter, $extSourceName, $attributesFromIdP, $identifierAttributes);
    if ($userExtSource === null) {
        throw new Exception(
            sprintf(
                "%sThere is no UserExtSource that could be used for user %s and IdP %s",
                UESUpdateHelper::DEBUG_PREFIX,
                $perunUserId,
                $extSourceName
            )
        );
    }

    $attributesFromPerun = UESUpdateHelper::getAttributesFromPerun($adapter, $attrMap, $userExtSource);
    $attributesToUpdate = UESUpdateHelper::getAttributesToUpdate(
        $attributesFromPerun,
        $attrMap,
        $serializedAttributes,
        $appendOnlyAttrs,
        $attributesFromIdP
    );

    if (UESUpdateHelper::updateUserExtSource($adapter, $userExtSource, $attributesToUpdate)) {
        Logger::debug(
            sprintf(
                "%sUpdating UES for user with userId: %s was successful. Updated attributes: %s",
                UESUpdateHelper::DEBUG_PREFIX,
                $perunUserId,
                json_encode($attributesToUpdate)
            )
        );
    }
} catch (\Exception $ex) {
    Logger::warning(
        sprintf(
            "%sUpdating UES for user with userId: %s was not successful: %s",
            UESUpdateHelper::DEBUG_PREFIX,
            $perunUserId,
            $ex->getMessage()
        )
    );
}
