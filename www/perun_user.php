<?php

declare(strict_types=1);

use SimpleSAML\Auth\State;
use SimpleSAML\Configuration;
use SimpleSAML\Module;
use SimpleSAML\Module\perun\Auth\Process\PerunUser;
use SimpleSAML\Module\perun\PerunConstants;
use SimpleSAML\XHTML\Template;

$config = Configuration::getInstance();
$t = new Template($config, PerunUser::TEMPLATE);

$stateId = $_REQUEST[PerunUser::PARAM_STATE_ID];
$state = State::loadState($stateId, PerunUser::STAGE);

$callback = Module::getModuleURL(PerunUser::CALLBACK, [
    PerunUser::PARAM_STATE_ID => $stateId,
]);

$data = $state[PerunUser::STAGE];

$t->data[PerunUser::PARAM_REGISTRATION_URL] = $data[PerunUser::PARAM_REGISTRATION_URL];
$params = [
    PerunConstants::TARGET_NEW => $callback,
    PerunConstants::TARGET_EXISTING => $callback,
    PerunConstants::TARGET_EXTENDED => $callback,
    PerunConstants::VO => $data[PerunUser::PARAM_REGISTRATION_VO],
];

if (!empty($data[PerunUser::PARAM_REGISTRATION_GROUP])) {
    $params[PerunConstants::GROUP] = $data[PerunUser::PARAM_REGISTRATION_GROUP];
}

$t->data[PerunUser::PARAM_PARAMS] = $params;

$t->show();
