<?php

declare(strict_types=1);

use SimpleSAML\Auth\ProcessingChain;
use SimpleSAML\Auth\State;
use SimpleSAML\Error\BadRequest;
use SimpleSAML\Module\perun\Auth\Process\EnsureVoAup;
use SimpleSAML\Module\perun\PerunConstants;

if (empty($_REQUEST[EnsureVoAup::PARAM_STATE_ID])) {
    throw new BadRequest('Missing required \'' . EnsureVoAup::PARAM_STATE_ID . '\' query parameter.');
}
$state = State::loadState($_REQUEST[EnsureVoAup::PARAM_STATE_ID], EnsureVoAup::STAGE);

$filterConfig = $state[PerunConstants::CONTINUE_FILTER_CONFIG];
$ensureVoAup = new EnsureVoAup($filterConfig, null);
$ensureVoAup->process($state);

// we have not been redirected, continue processing
ProcessingChain::resumeProcessing($state);
