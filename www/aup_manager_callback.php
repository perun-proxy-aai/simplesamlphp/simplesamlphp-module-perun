<?php

declare(strict_types=1);

use Firebase\JWT\JWT;
use SimpleSAML\Auth\ProcessingChain;
use SimpleSAML\Auth\State;
use SimpleSAML\Configuration;
use SimpleSAML\Error\BadRequest;
use SimpleSAML\Error\NoState;
use SimpleSAML\Module\perun\Auth\Process\AUPManager;
use SimpleSAML\Module\perun\Auth\Process\PerunAup;
use SimpleSAML\Module\perun\PerunConstants;
use SimpleSAML\XHTML\Template;

if (!array_key_exists(AUPManager::PARAM_NONCE, $_REQUEST)) {
    throw new BadRequest('Missing required nonce query parameter.');
}

$originalNonce = urldecode($_REQUEST[AUPManager::PARAM_NONCE]);
$nonceArray = explode(';', $originalNonce);
$id = $nonceArray[0];
$state = State::loadState($id, AUPManager::STAGE);
if (null === $state) {
    throw new NoState();
}

$userId = $state[AUPManager::STAGE][AUPMANAGER::PARAM_USER_ID];
$signingKey = $state[AUPManager::STAGE][AUPManager::SIGNING_KEY];
$resultCheckUrl = $state[AUPManager::STAGE][AUPManager::RESULT_CHECK_URL];
$signingAlg = $state[AUPManager::STAGE][AUPManager::SIGNING_ALG];

$payload = [
    AUPManager::PARAM_NONCE  => $originalNonce,
    AUPMANAGER::PARAM_USER_ID => $userId,
];

$jwt = JWT::encode($payload, $signingKey, $signingAlg);


$resultCheckUrl = $resultCheckUrl . "/" . $jwt;
$response_json = file_get_contents($resultCheckUrl);
$response = json_decode($response_json, true);

if ($response[AUPManager::PARAM_NONCE] != $originalNonce) {
    throw new \SimpleSAML\Error\Exception(PerunAup::DEBUG_PREFIX . "Error when checking AUP response.");
}

if ($response['result'] == 'okay') {
    ProcessingChain::resumeProcessing($state);
} elseif ($response['result'] == 'nok') {
    $config = Configuration::getInstance();
    $t = new Template($config, AupManager::NOT_ACCEPTED_TPL);
    $t->data[PerunConstants::SP_METADATA] = $state[PerunConstants::SP_METADATA];
    $t->show();
} else {
    throw new \SimpleSAML\Error\Exception(PerunAup::DEBUG_PREFIX . "Error when checking AUP response.");
}
