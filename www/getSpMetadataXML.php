<?php

declare(strict_types=1);

use SAML2\Constants;
use SimpleSAML\Logger;
use SimpleSAML\Metadata\SAMLBuilder;
use SimpleSAML\Module\perun\MetadataFromPerun;

$fetch = new MetadataFromPerun();
$metadata = $fetch->getAllMetadata();
$result = '<?xml version="1.0" encoding="utf-8" standalone="no"?>' . PHP_EOL;
$result .= '<EntitiesDescriptor xmlns="urn:oasis:names:tc:SAML:2.0:metadata">';
foreach ($metadata as $entityId => $metaArray20) {
    $metaArray20['metadata-set'] = 'saml20-sp-remote';
    if (empty($metaArray20['AssertionConsumerService'])) {
        Logger::warning('Skipping XML metadata generation for SP ' . $entityId . ' without ACS');
        continue;
    }
    $metaBuilder = new SAMLBuilder($entityId);
    $metaBuilder->addMetadataSP20($metaArray20);
    try {
        $xml = $metaBuilder->getEntityDescriptorText();
        $xml = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $xml);
        $result .= $xml;
    } catch (\Exception $e) {
        Logger::warning('Could not create XML metadata for ' . $entityId . ': ' . $e->getMessage());
    }
}
$result .= '</EntitiesDescriptor>';
header('Content-Type: application/samlmetadata+xml');
header('Content-Disposition: attachment; filename="metadata.xml"');
echo $result;
