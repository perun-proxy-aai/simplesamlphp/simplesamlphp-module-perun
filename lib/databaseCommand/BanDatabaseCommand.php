<?php

declare(strict_types=1);

namespace SimpleSAML\Module\perun\databaseCommand;

use SimpleSAML\Logger;
use SimpleSAML\Module\perun\databaseCommand\DatabaseCommand;

class BanDatabaseCommand extends DatabaseCommand
{
    private const STAGE = 'perun:BanDatabaseCommand';

    private const DEBUG_PREFIX = self::STAGE . ' - ';

    public function getBanForID($table, $userID, $colName)
    {
        $query = 'SELECT id FROM ' . $this->q() . $table . $this->q()
          . ' WHERE ' . $this->q() . $colName . $this->q() . '=:userid';
        return parent::read($query, ['userid' => $userID])->fetchColumn();
    }
}
