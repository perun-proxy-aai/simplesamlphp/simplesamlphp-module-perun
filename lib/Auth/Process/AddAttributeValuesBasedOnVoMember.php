<?php

declare(strict_types=1);

namespace SimpleSAML\Module\perun\Auth\Process;

use Exception;
use SimpleSAML\Auth\ProcessingFilter;
use SimpleSAML\Configuration;
use SimpleSAML\Logger;
use SimpleSAML\Module\perun\Adapter;
use SimpleSAML\Module\perun\PerunConstants;

/**
 * Add attribute values based on VO membership.
 */
class AddAttributeValuesBasedOnVoMember extends ProcessingFilter
{
    public const STAGE = 'perun:AddAttributeValuesBasedOnVoMember';

    public const DEBUG_PREFIX = self::STAGE . ' - ';

    public const MEMBERSHIP_MAP_PROPNAME = 'membership_map';
    public const ATTRIBUTE_NAME_PROPNAME = 'destination_attribute';
    public const INTERFACE_PROPNAME = 'interface';

    private $filterConfig;
    private $membershipMap;

    private $adapter;
    private $attributeName;

    public function __construct($config, $reserved)
    {
        parent::__construct($config, $reserved);
        $this->filterConfig = Configuration::loadFromArray($config);

        $this->membershipMap = $this->filterConfig->getArray(self::MEMBERSHIP_MAP_PROPNAME, []);
        if (empty($this->membershipMap)) {
            throw new Exception(
                self::DEBUG_PREFIX . 'Invalid configuration: no vo ID to affiliations map has '
                . 'been configured. Use option \'' . self::MEMBERSHIP_MAP_PROPNAME .
                '\' to configure the map, where key is the voId and value is an array of attr values to add.'
            );
        }
        $this->attributeName = $this->filterConfig->getString(self::ATTRIBUTE_NAME_PROPNAME, null);
        if (empty($this->attributeName)) {
            throw new Exception(
                self::DEBUG_PREFIX . 'Invalid configuration: no attribute name where to store result'
                . 'affiliations has been configured. Use option \'' . self::ATTRIBUTE_NAME_PROPNAME .
                '\' to configure the destination attribute name.'
            );
        }
        $interface = $this->filterConfig->getValueValidate(
            self::INTERFACE_PROPNAME,
            [Adapter::RPC, Adapter::LDAP],
            Adapter::RPC
        );
        $this->adapter = Adapter::getInstance($interface);
    }

    public function process(&$request)
    {
        $user = $request[PerunConstants::PERUN][PerunConstants::USER] ?? null;
        if (empty($user)) {
            throw new \SimpleSAML\Error\Exception(
                self::DEBUG_PREFIX . 'missing mandatory field \'perun.user\' in request.' .
                'Hint: Did you configured PerunIdentity filter before this filter?'
            );
        }

        $userVoIds = $this->adapter->getUserVoIds((int) $user->getId());
        $result = [];
        if (!empty($userVoIds)) {
            foreach ($this->membershipMap as $voId => $attributeValues) {
                if (in_array($voId, $userVoIds)) {
                    $result = array_merge($attributeValues, $result);
                }
            }
            $result = array_unique($result);
        }

        if (empty($result)) {
            Logger::debug(
                self::DEBUG_PREFIX . ' user is not in any of the specified VOs, not adding any attr values'
            );
        }
        $request[PerunConstants::ATTRIBUTES][$this->attributeName] = $result;
    }
}
