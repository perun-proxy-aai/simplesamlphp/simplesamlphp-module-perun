<?php

declare(strict_types=1);

namespace SimpleSAML\Module\perun\Auth\Process;

use SimpleSAML\Auth\ProcessingFilter;
use SimpleSAML\Configuration;
use SimpleSAML\Logger;
use SimpleSAML\Module\perun\Adapter;

/**
 * Generate eduPersonAffiliation and eduPersonPrimaryAffiliation from groups in Perun IdM.
 * Inspired by https://infrastructure.tamu.edu/directory/attribute/attribute_eduPersonPrimaryAffiliation.html
 * and https://www.eduid.cz/cs/tech/attributes/edupersonaffiliation
 */
class PerunAffiliation extends ProcessingFilter
{
    public const CONFIG_FILE_NAME = 'module_perun.php';

    public const ENTITY_ID_ATTR = 'entityIdAttr';

    private const AFFILIATIONS = [
        'student', 'employee', 'member', 'staff', 'faculty', 'alum', 'affiliate', 'library-walk-in'
    ];

    private $perunProxyIdPEntityID;

    private $interface = Adapter::RPC;

    private $eduPersonAffiliationAttribute = 'eduPersonAffiliation';

    private $eduPersonPrimaryAffiliationAttribute = 'eduPersonPrimaryAffiliation';

    private $affiliationCapabilityPrefix = 'res:proxyidp:affiliation:';

    private $permanentEmployeeCapability = 'res:proxyidp:employee-permanent';

    private $fallbackAffiliation = 'affiliate';

    public function __construct($config, $reserved)
    {
        parent::__construct($config, $reserved);
        $config = Configuration::loadFromArray($config);
        $this->perunProxyIdPEntityID = $config->getString('perunProxyIdPEntityID');
        $this->interface = $config->getValueValidate(
            'interface',
            [Adapter::RPC, Adapter::LDAP],
            Adapter::RPC
        );
        $this->eduPersonAffiliationAttribute = $config->getString(
            'eduPersonAffiliationAttribute',
            $this->eduPersonAffiliationAttribute
        );
        $this->eduPersonPrimaryAffiliationAttribute = $config->getString(
            'eduPersonPrimaryAffiliationAttribute',
            $this->eduPersonPrimaryAffiliationAttribute
        );
        $this->affiliationCapabilityPrefix = $config->getString(
            'affiliationCapabilityPrefix',
            $this->affiliationCapabilityPrefix
        );
        $this->permanentEmployeeCapability = $config->getString(
            'permanentEmployeeCapability',
            $this->permanentEmployeeCapability
        );
        $this->fallbackAffiliation = $config->getString('fallbackAffiliation', $this->fallbackAffiliation);
    }

    public function process(&$request)
    {
        if (empty($request['perun']['user'])) {
            throw new \Exception(
                'perun:PerunAffiliation: Perun user is missing (PerunIdentity must run before this filter)'
            );
        }
        try {
            $configuration = Configuration::getConfig(self::CONFIG_FILE_NAME);
            $entityIdAttr = $configuration->getString(self::ENTITY_ID_ATTR, 'perunFacilityAttr_entityID');
        } catch (\Exception $e) {
            $entityIdAttr = 'perunFacilityAttr_entityID';
        }
        $adapter = Adapter::getInstance($this->interface);
        $groups = $adapter->getUsersGroupsOnFacility(
            $this->perunProxyIdPEntityID,
            $request['perun']['user']->getId(),
            $entityIdAttr
        );
        $capabilities = $adapter->getResourceCapabilities($this->perunProxyIdPEntityID, $groups, $entityIdAttr);
        $is_permanent_employee = in_array($this->permanentEmployeeCapability, $capabilities, true);
        $affiliations = str_replace($this->affiliationCapabilityPrefix, '', $capabilities);
        $affiliations = array_intersect(self::AFFILIATIONS, $affiliations);
        if ($this->fallbackAffiliation) {
            $affiliations[] = $this->fallbackAffiliation;
        }
        $affiliations = array_unique($affiliations);
        $request["Attributes"][$this->eduPersonAffiliationAttribute] = $affiliations;
        $primaryAffiliation = self::getPrimaryAffiliation($affiliations, $is_permanent_employee);
        if ($primaryAffiliation) {
            $request["Attributes"][$this->eduPersonPrimaryAffiliationAttribute] = [$primaryAffiliation];
        }
    }

    private static function getPrimaryAffiliation($affiliations, $is_permanent_employee)
    {
        if (in_array('faculty', $affiliations, true)) {
            return 'faculty';
        }
        if (in_array('staff', $affiliations, true) && $is_permanent_employee) {
            return 'staff';
        }
        if (in_array('student', $affiliations, true)) {
            return 'student';
        }
        if (in_array('staff', $affiliations, true)) {
            return 'staff';
        }
        if (in_array('employee', $affiliations, true)) {
            return 'employee';
        }
        if (in_array('member', $affiliations, true)) {
            return 'member';
        }
        if (in_array('alum', $affiliations, true)) {
            return 'alum';
        }
        if (in_array('affiliate', $affiliations, true)) {
            return 'affiliate';
        }
        if (in_array('library-walk-in', $affiliations, true)) {
            return 'library-walk-in';
        }
        return null;
    }
}
