<?php

declare(strict_types=1);

namespace SimpleSAML\Module\perun\Auth\Process;

use DateTime;
use donatj\UserAgent\UserAgentParser;
use SimpleSAML\Auth\ProcessingFilter;
use SimpleSAML\Configuration;
use SimpleSAML\Module\perun\PerunConstants;
use SimpleSAML\Session;
use SimpleSAML\Logger;
use SimpleSAML\Error\Exception;
use SimpleSAML\Module\perun\EntitlementUtils;
use SimpleSAML\Module\perun\databaseCommand\AuthEventDatabaseCommand;
use GeoIp2\Database\Reader;

class AuthEventLogging extends ProcessingFilter
{
    private const STAGE = 'perun:AuthEventLogging';

    private const DEBUG_PREFIX = self::STAGE . ' - ';

    public const CONFIG_FILE_NAME = 'module_perun.php';

    public const API_SERVICE_NAME = 'serviceName';

    public const API_SERVICE_IDENTIFIER = 'serviceIdentifier';

    public const API_IDP_NAME = 'idpName';

    public const API_IDP_IDENTIFIER = 'idpIdentifier';

    /// TABLES --------------------------------------------------
    private const TABLE_AUTH_EVENT = 'auth_event_logging';

    // identifier + name
    private const TABLE_LOGGING_IDP = 'logging_idp';

    private const TABLE_LOGGING_SP = 'logging_sp';

    // value
    private const TABLE_SESSION_ID = 'session_id_values';

    private const TABLE_REQUESTED_ACRS = 'requested_acrs_values';

    private const TABLE_UPSTREAM_ACRS = 'upstream_acrs_values';

    private const TABLE_USER_AGENT_RAW = 'user_agent_raw_values';

    private const TABLE_USER_AGENT = 'user_agent_values';
    /// ----------------------------------------------------------

    /// ACRs -----------------------------------------------------
    // REFEDS profile for SFA.
    public const REFEDS_SFA = 'https://refeds.org/profile/sfa';

    // REFEDS profile for MFA.
    public const REFEDS_MFA = 'https://refeds.org/profile/mfa';

    // Microsoft authentication context for MFA.
    public const MS_MFA = 'http://schemas.microsoft.com/claims/multipleauthn';

    // Contexts trusted as multifactor authentication, in the order of preference (for replies).
    public const MFA_CONTEXTS = [self::REFEDS_MFA, self::MS_MFA];

    // Contexts trusted as password authentication, in the order of preference (for replies).
    public const PASSWORD_CONTEXTS = [self::REFEDS_SFA];
    /// ----------------------------------------------------------

    private const ENTITY_ID = 'entityid';

    private const GEO_CITY_DB_FILE = 'geoCityDBFile';

    private const GEO_COUNTRY_DB_FILE = 'geoCountryDBFile';

    private const USER_ID_ATTR = 'userIdAttribute';

    private $filterConfig;

    private $entityId;

    private $perunModuleConfig;

    private $typeFilterArray;

    private $tokenTypeAttr;

    public function __construct($in_config, $reserved)
    {
        parent::__construct($in_config, $reserved);

        $this->perunModuleConfig = Configuration::getConfig(self::CONFIG_FILE_NAME);
        $this->filterConfig = Configuration::loadFromArray($in_config);
        $this->entityId = $this->filterConfig->getValue(self::ENTITY_ID, null);
        $this->typeFilterArray = $this->perunModuleConfig->getArray('type_filter_array', $this->typeFilterArray);
        $this->tokenTypeAttr = $this->perunModuleConfig->getString('token_type_attr', $this->tokenTypeAttr);
    }

    public function process(&$request)
    {
        // Create connection via DatabaseConfig
        $dbCmd = new AuthEventDatabaseCommand();
        // Retrieve all needed variables
        $dateTimeObject = new DateTime();
        $dateTime = $dateTimeObject->format('Y-m-d H:i:s');

        $userId = $this->getUserId($request);

        $clientIpAddr = $_SERVER['REMOTE_ADDR'];
        $userAgentRaw = $_SERVER['HTTP_USER_AGENT'];

        $userAgent = $this->getBrowserFromUA();
        Logger::debug(self::DEBUG_PREFIX . $userAgent);

        $sessionId = $this->getSessionId();
        list($country, $city) = $this->getLocation($clientIpAddr);
        list($requestedACRs, $upstreamACRs) = $this->getAcrs($request);
        list($idpIdentifier, $idpName, $spIdentifier, $spName) = $this->getEntities($request);

        // Inserting into side tables
        $idpTableId = $dbCmd->getIdFromIdentifier(self::TABLE_LOGGING_IDP, $idpIdentifier, $idpName);
        $spTableId = $dbCmd->getIdFromIdentifier(self::TABLE_LOGGING_SP, $spIdentifier, $spName);

        $sessionTableId = $dbCmd->getIdFromForeignTable(self::TABLE_SESSION_ID, $sessionId);
        $requestedACRsTableId = $dbCmd->getIdFromForeignTable(
            self::TABLE_REQUESTED_ACRS,
            json_encode($requestedACRs)
        );
        $upstreamACRsTableId = $dbCmd->getIdFromForeignTable(self::TABLE_UPSTREAM_ACRS, $upstreamACRs);
        $userAgentRawTableId = $dbCmd->getIdFromForeignTable(self::TABLE_USER_AGENT_RAW, $userAgentRaw);
        $userAgentTableId = $dbCmd->getIdFromForeignTable(self::TABLE_USER_AGENT, $userAgent);
        $localMFAPerformed = $this->getLocalMFAStatus($request);

        $dbCmd->insertIntoAuthTable(self::TABLE_AUTH_EVENT, [
            'day' => $dateTime,
            'user' => $userId,
            'idp_id' => $idpTableId,
            'sp_id' => $spTableId,
            'ip_address' => $clientIpAddr,
            'geolocation_city' => $city,
            'geolocation_country' => $country,
            'local_mfa_performed' => $localMFAPerformed,
            'session_id' => $sessionTableId,
            'requested_acrs_id' => $requestedACRsTableId,
            'upstream_acrs_id' => $upstreamACRsTableId,
            'user_agent_raw_id' => $userAgentRawTableId,
            'user_agent_id' => $userAgentTableId,
            'user_id' => $request[PerunConstants::PERUN][PerunConstants::USER]->getId(),
        ]);
    }

    private function getSessionId()
    {
        $session = Session::getSessionFromRequest();
        return $session->getSessionId();
    }

    private function getBrowserFromUA($ua = null)
    {
        try {
            $parser = new UserAgentParser();
            $info = $parser->parse($ua);
            $browserInfo = $info->platform() . ', ' . $info->browser();
        } catch (\InvalidArgumentException $e) {
            $browserInfo = 'unknown|unknown';
        }

        return $browserInfo;
    }

    // Retrieve information about country and city of $ip ip address
    private function getLocation($ip)
    {
        $country = '';
        $city = '';

        $geoCityDBReader = new Reader($this->perunModuleConfig->getValue(self::GEO_CITY_DB_FILE));
        $geoCountryDBReader = new Reader($this->perunModuleConfig->getValue(self::GEO_COUNTRY_DB_FILE));

        if ($geoCountryDBReader != null) {
            try {
                $record = $geoCountryDBReader->country($ip);
                $country = $record->country->isoCode;
                Logger::debug(self::DEBUG_PREFIX . "Country: " . $country);
            } catch (Exception | \Exception $e) {
                Logger::error(self::DEBUG_PREFIX . $e->getMessage());
            }
        }

        if ($geoCityDBReader != null) {
            try {
                $record = $geoCityDBReader->city($ip);
                $city = $record->city->name;
                Logger::debug(self::DEBUG_PREFIX . "City: " . $city);
            } catch (Exception | \Exception $e) {
                Logger::error(self::DEBUG_PREFIX . $e->getMessage());
            }
        }

        return [$country, $city];
    }

    private function getUserId($request)
    {
        $idAttribute = $this->perunModuleConfig->getValue(self::USER_ID_ATTR, 'uid');

        return isset($request['Attributes'][$idAttribute]) ? $request['Attributes'][$idAttribute][0] : '';
    }

    // get info about SP and IdP
    private function getEntities($request): array
    {
        $idpIdentifier = "";
        $idpName = "";
        $spIdentifier = "";
        $spName = "";

        $idpIdentifier = $request['Attributes']['sourceIdPEntityID'][0];
        $idpName = $request['Attributes']['sourceIdPName'][0];
        $spIdentifier = $this->getSpIdentifier($request);
        $spName = $this->getSpName($request);

        return [$idpIdentifier, $idpName, $spIdentifier, $spName];
    }

    private function getSpIdentifier($request)
    {
        if ($this->entityId === null) {
            $this->entityId = EntitlementUtils::getSpEntityId($request);
        } elseif (is_callable($this->entityId)) {
            $this->entityId = call_user_func($this->entityId, $request);
        } elseif (!is_string($this->entityId)) {
            throw new Exception(
                'perun:PerunEntitlement: invalid configuration option entityID. ' . 'It must be a string or a callable.'
            );
        }

        return $this->entityId;
    }

    private function getSpName($request)
    {
        $displayName = $request['Destination']['UIInfo']['DisplayName']['en'] ?? '';
        if (empty($displayName)) {
            $displayName = $request['Destination']['name']['en'] ?? '';
        }

        return $displayName;
    }

    private function getMFAForUid($state)
    {
        $result = [];
        if (!empty($state['Attributes']['mfaTokens'])) {
            foreach ($state['Attributes']['mfaTokens'] as $mfaToken) {
                if (is_string($mfaToken)) {
                    $mfaToken = json_decode($mfaToken, true);
                }

                foreach ($this->typeFilterArray as $type => $method) {
                    if ($mfaToken['revoked'] === false && $mfaToken[$this->tokenTypeAttr] === $type) {
                        $result[] = self::REFEDS_MFA;
                        break;
                    }
                }
                if (!empty($result)) {
                    break;
                }
            }
        }
        $result[] = self::REFEDS_SFA;

        return $result;
    }

    public static function fetchContextFromUpstreamIdp($state)
    {
        if (
            isset($state['saml:RequestedAuthnContext']['AuthnContextClassRef'])
            && isset($state['saml:sp:AuthnContext'])
        ) {
            $upstreamIdpAuthnContextClassRef = $state['saml:sp:AuthnContext'];
            $requestedContexts = $state['saml:RequestedAuthnContext']['AuthnContextClassRef'];
            if (
                in_array($upstreamIdpAuthnContextClassRef, $requestedContexts, true)
                && !empty($upstreamIdpAuthnContextClassRef)
            ) {
                return $upstreamIdpAuthnContextClassRef;
            }
        }

        // IdP returned a context which was not requested, ignore it
        return "";
    }

    private function getLocalMFAStatus($request)
    {
        $localMFAPerformed = empty($request['authswitcher_mfa_performed']) ? 'false' : 'true';
        return $localMFAPerformed;
    }

    private function getAcrs($request)
    {
        $requestedACRs = $this->getMFAForUid($request);
        $upstreamACRs = $this->fetchContextFromUpstreamIdp($request);
        return [$requestedACRs, $upstreamACRs];
    }
}
