<?php

namespace SimpleSAML\Module\perun\Auth\Process;

use SimpleSAML\Auth\ProcessingFilter;
use SimpleSAML\Error\Exception;
use SimpleSAML\Configuration;
use SimpleSAML\Module\perun\PerunConstants;

/**
 * AuthProcFilter to compute hashes from selected attributes by config and to storing result in configured attribute.
 */
class AdditionalIdentifiers extends ProcessingFilter
{
    public const STAGE = 'perun:AdditionalIdentifiers';

    public const DEBUG_PREFIX = self::STAGE . ' - ';

    public const HASH_SALT = "hashSalt";

    public const HASH_FUNCTION = "hashFunction";

    public const HASH_ATTRS = "hashAttributes";

    public const ADDITIONAL_IDENTIFIERS_ATTR = 'additionalIdentifiersAttribute';

    private $hashSalt;

    private $hashFunction;

    private $hashAttributes;

    private $additionalIdentifiersAttr;


    public function __construct($config, $reserved)
    {
        parent::__construct($config, $reserved);
        $filterConfig = Configuration::loadFromArray($config);
        $this->hashSalt = $filterConfig->getString(self::HASH_SALT, "");
        $this->hashFunction = $filterConfig->getString(self::HASH_FUNCTION, "");
        $this->hashAttributes = $filterConfig->getArray(self::HASH_ATTRS, []);
        $this->additionalIdentifiersAttr = $filterConfig->getString(self::ADDITIONAL_IDENTIFIERS_ATTR, "");

        if (!empty($this->hashFunction) && !in_array($this->hashFunction, hash_algos())) {
            throw new Exception(
                self::DEBUG_PREFIX . "'" . $this->hashFunction . "' hash function is not supported, please 
                use one of following: " . implode(",", hash_algos())
            );
        }
        if (empty($this->additionalIdentifiersAttr)) {
            throw new Exception(
                self::DEBUG_PREFIX . "missing 'additionalIdentifiersAttribute' configuration option of
                attribute name for storing AUIDs"
            );
        }

        if (empty($this->hashAttributes)) {
            throw new Exception(
                self::DEBUG_PREFIX . "missing 'hashAttributes' configuration option for selecting attributes 
                to generate AUIDs"
            );
        }
    }

    public function process(&$request)
    {
        $attr_values_to_hash = $this->getAttributesValuesToHash($request);

        if (empty($attr_values_to_hash)) {
            //Probably should do redirect to error page because no hashes can be generated
            $request[PerunConstants::ATTRIBUTES][$this->additionalIdentifiersAttr] = [];
        } else {
            $hashed_attr_values = [];
            foreach ($attr_values_to_hash as $value_to_hash) {
                $value_to_hash = str_replace(',', ', ', $value_to_hash);
                $value_to_hash = str_replace('\\/', '/', $value_to_hash);
                $value_to_hash .= $this->hashSalt;
                if (!empty($this->hashFunction)) {
                    $value_to_hash = hash($this->hashFunction, $value_to_hash);
                }
                $hashed_attr_values[] = $value_to_hash;
            }
            $request[PerunConstants::ATTRIBUTES][$this->additionalIdentifiersAttr] = $hashed_attr_values;
        }
    }

    private function getAttributesValuesToHash(&$request): array
    {
        $result_attr_values = [];
        foreach ($this->hashAttributes as $attribute_config) {
            $value_to_hash = $this->buildValuesToHash($request, $attribute_config);
            if (!empty($value_to_hash)) {
                $result_attr_values[] = $value_to_hash;
            }
        }

        return $result_attr_values;
    }

    private function buildValuesToHash($request, $attr_cfg): string
    {
        $to_hash = [];
        foreach ($attr_cfg as $attr_name => $regex) {
            $attribute_values = "";
            if ($attr_name == "requester") {
                $attribute_values = $request['saml:RequesterID'];
            } elseif (isset($request[PerunConstants::ATTRIBUTES][$attr_name])) {
                $attribute_values = $request[PerunConstants::ATTRIBUTES][$attr_name];
            }
            if (empty($attribute_values)) {
                return "";
            }

            $cur_value = [];
            if (is_array($attribute_values)) {
                foreach ($attribute_values as $attr_val) {
                    if (preg_match($regex, $attr_val)) {
                        $cur_value[] = $attr_val;
                    }
                }
                if (empty($cur_value)) {
                    return "";
                }
                $to_hash[] = $cur_value;
            } else {
                if (preg_match($regex, $attribute_values)) {
                    $cur_value[] = $attribute_values;
                } else {
                    return "";
                }
                $to_hash[] = $cur_value;
            }
        }
        if (empty($to_hash)) {
            return "";
        }
        return json_encode($to_hash);
    }
}
