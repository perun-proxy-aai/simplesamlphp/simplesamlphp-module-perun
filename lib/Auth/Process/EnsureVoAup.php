<?php

declare(strict_types=1);

namespace SimpleSAML\Module\perun\Auth\Process;

use SimpleSAML\Auth\ProcessingFilter;
use SimpleSAML\Auth\State;
use SimpleSAML\Configuration;
use SimpleSAML\Error\Exception;
use SimpleSAML\Logger;
use SimpleSAML\Module;
use SimpleSAML\Module\perun\Adapter;
use SimpleSAML\Module\perun\CommonUtils;
use SimpleSAML\Module\perun\PerunConstants;
use SimpleSAML\Utils\HTTP;

/**
 * Class checks if the user has approved given aup of the VO on facility, and forwards to approval page if not.
 * Should be run after EnsureVoMember filter
 */
class EnsureVoAup extends ProcessingFilter
{
    public const STAGE = 'perun:EnsureVoAup';

    public const DEBUG_PREFIX = self::STAGE . ' - ';

    public const CALLBACK = 'perun/ensure_vo_aup_callback.php';

    public const REDIRECT = 'perun/ensure_vo_aup.php';

    //REUSED
    public const TEMPLATE = 'perun:perun-aup-tpl.php';

    public const PARAM_STATE_ID = PerunConstants::STATE_ID;

    public const PARAM_APPROVAL_URL = 'approvalUrl';
    public const PARAM_APPROVAL_VO = 'approvalVo';
    public const PARAM_APPROVAL_GROUP = 'approvalGroup';

    public const PARAM_PARAMS = 'params';

    public const INTERFACE = 'interface';

    public const AUP_ATTR = 'user_attribute';

    public const AUP_VALUE = 'aup_value';

    public const PERUN_APPROVAL_URL = 'perun_approval_url';
    public const PERUN_APPROVAL_GROUP = 'perun_approval_group';

    public const TRIGGER_ATTR = 'trigger_attr';

    public const VO_DEFS_ATTR = 'vo_defs_attr';

    public const VO_AUP_CONF = 'vo_aup_conf';


    private $adapter;

    private $triggerAttr;

    private $voDefsAttr;

    private $voAupConf;

    private $perunApprovalUrl;

    private $config;

    private $filterConfig;

    public function __construct($config, $reserved)
    {
        parent::__construct($config, $reserved);
        $this->config = $config;
        $this->filterConfig = Configuration::loadFromArray($config);

        $interface = $this->filterConfig->getString(self::INTERFACE, Adapter::RPC);
        $this->adapter = Adapter::getInstance($interface);

        $this->triggerAttr = $this->filterConfig->getString(self::TRIGGER_ATTR, '');

        if (empty($this->triggerAttr)) {
            throw new Exception(self::DEBUG_PREFIX . 'Missing configuration option \'' . self::TRIGGER_ATTR . '\'');
        }

        $this->voDefsAttr = $this->filterConfig->getString(self::VO_DEFS_ATTR, '');

        if (empty($this->voDefsAttr)) {
            throw new Exception(self::DEBUG_PREFIX . 'Missing configuration option \'' . self::VO_DEFS_ATTR . '\'');
        }

        $this->voAupConf = $this->filterConfig->getArray(self::VO_AUP_CONF, []);

        if (empty($this->voAupConf)) {
            throw new Exception(self::DEBUG_PREFIX . 'Missing configuration option \'' . self::VO_AUP_CONF . '\'');
        }

        $this->perunApprovalUrl = $this->filterConfig->getString(self::PERUN_APPROVAL_URL, null);
        if (empty($this->perunApprovalUrl)) {
            throw new Exception(
                self::DEBUG_PREFIX . 'Missing configuration option \'' . self::PERUN_APPROVAL_URL . '\''
            );
        }

        foreach ($this->voAupConf as $key => $value) {
            if (!array_key_exists(self::AUP_VALUE, $value)) {
                throw new Exception(
                    self::DEBUG_PREFIX . 'Missing configuration \'' . self::AUP_VALUE . '\' for VO \'' . $key . '\''
                );
            } elseif (!array_key_exists(self::AUP_ATTR, $value)) {
                throw new Exception(
                    self::DEBUG_PREFIX . 'Missing configuration \'' . self::AUP_ATTR . '\' for VO \'' . $key . '\''
                );
            } elseif (!array_key_exists(self::PERUN_APPROVAL_GROUP, $value)) {
                throw new Exception(
                    self::DEBUG_PREFIX .
                    'Missing configuration \'' .
                    self::PERUN_APPROVAL_GROUP .
                    '\' for VO \'' .
                    $key .
                    '\''
                );
            }
        }
    }

    public function process(&$request)
    {
        assert(is_array($request));
        assert(!empty($request[PerunConstants::PERUN][PerunConstants::USER]));

        if (empty($request[PerunConstants::PERUN][PerunConstants::USER])) {
            throw new Exception(
                self::DEBUG_PREFIX . 'Request does not contain Perun user. Did you configure ' . PerunUser::STAGE .
                ' filter before this filter in the processing chain?'
            );
        }
        $user = $request[PerunConstants::PERUN][PerunConstants::USER];


        if (!isset($request['SPMetadata']['entityid'])) {
            throw new Exception(
                self::DEBUG_PREFIX . 'Cannot find entityID of remote SP. ' .
                'hint: Do you have this filter in IdP context?'
            );
        }

        $facility = CommonUtils::getFacilityFromRequest($request, $this->adapter);

        if ($facility === null) {
            Logger::debug(self::DEBUG_PREFIX . 'skip execution - no facility provided');
            return;
        }

        $attrValues = $this->adapter->getFacilityAttributesValues(
            $facility,
            [$this->voDefsAttr, $this->triggerAttr]
        );
        $triggerAttrValue = $attrValues[$this->triggerAttr];
        if ($triggerAttrValue === null || $triggerAttrValue === false) {
            Logger::debug(
                self::DEBUG_PREFIX . 'skip execution - attribute ' . self::TRIGGER_ATTR . ' is null or false'
            );
            return;
        }
        $voShortName = $attrValues[$this->voDefsAttr];
        if (empty($voShortName)) {
            Logger::debug(
                self::DEBUG_PREFIX . 'skip execution - attribute ' . self::VO_DEFS_ATTR . ' has null or no value'
            );
            return;
        }

        $aupConfig = $this->voAupConf[$voShortName];

        if (empty($aupConfig)) {
            Logger::warning(
                self::DEBUG_PREFIX . 'Accepting of AUP for VO \'' . $voShortName . '\' is not configured.'
            );
        }

        $aupAttrName = $aupConfig[self::AUP_ATTR];
        $aupExpectedValue = $aupConfig[self::AUP_VALUE];

        $aupAttrValue = null;
        $userAttributesValues = $this->adapter->getUserAttributesValues($user, [$aupAttrName]);
        if (empty($userAttributesValues) || empty($userAttributesValues[$aupAttrName])) {
            Logger::warning(
                self::DEBUG_PREFIX . 'Attribute \'' . $aupAttrName . '\' is empty. Probably could not be '
                . 'fetched. Redirecting user to approve AUP.'
            );
        } else {
            $aupAttrValue = $userAttributesValues[$aupAttrName];
        }

        if ($aupAttrValue === $aupExpectedValue) {
            Logger::info(
                self::DEBUG_PREFIX . 'User approved AUP did match the expected value, continue processing.'
            );
            return;
        }
        Logger::info(
            self::DEBUG_PREFIX . 'User did not approve the expected AUP. Expected value \''
            . $aupExpectedValue . '\', actual value \'' . $aupAttrValue . '\'. Redirecting user to AUP approval page.'
        );
        $this->redirect($request, $aupConfig, $voShortName);
    }

    private function redirect(&$request, $aupConfig, $voShortName): void
    {
        $request[PerunConstants::CONTINUE_FILTER_CONFIG] = $this->config;

        $request[self::STAGE] = [
            self::PARAM_APPROVAL_URL => $this->perunApprovalUrl,
            self::PARAM_APPROVAL_VO => $voShortName,
            self::PARAM_APPROVAL_GROUP => $aupConfig[self::PERUN_APPROVAL_GROUP],
        ];
        $stateId = State::saveState($request, self::STAGE);

        $redirectUrl = Module::getModuleURL(self::REDIRECT, [self::PARAM_STATE_ID => $stateId]);
        Logger::debug(self::DEBUG_PREFIX . 'Redirecting to \'' . $redirectUrl . '\'');
        HTTP::redirectTrustedURL($redirectUrl);
    }
}
