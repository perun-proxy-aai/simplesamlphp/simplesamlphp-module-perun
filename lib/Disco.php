<?php

declare(strict_types=1);

namespace SimpleSAML\Module\perun;

use SimpleSAML\Auth\State;
use SimpleSAML\Configuration;
use SimpleSAML\Error\Exception;
use SimpleSAML\Logger;
use SimpleSAML\Module;
use SimpleSAML\Module\authswitcher\DiscoUtils;
use SimpleSAML\Module\discopower\PowerIdPDisco;
use SimpleSAML\Module\perun\model\WarningConfiguration;
use SimpleSAML\Utils\HTTP;
use SAML2\Constants;

/**
 * This class implements a IdP discovery service.
 *
 * This module extends the DiscoPower IdP disco handler, so it needs to be avaliable and enabled and configured.
 *
 * Supports hiding IdPs from discovery per SP.
 * If you want to block IdPs, please modify metadata directly.
 * Either remove individual IdPs from metadata or configure metarefresh to skip them in config-metarefresh.php.
 */
class Disco extends PowerIdPDisco
{
    private const DEBUG_PREFIX = 'perun:Disco - ';

    public const CONFIG_FILE_NAME = 'module_perun.php';

    public const URN_CESNET_PROXYIDP_IDPENTITYID = 'urn:cesnet:proxyidp:idpentityid:';
    public const URN_CESNET_PROXYIDP_ONLY_ALLOWED_IDPS = 'urn:cesnet:proxyidp:only_allowed_idps:';
    public const URN_CESNET_PROXYIDP_BLOCKED_IPDS = 'urn:cesnet:proxyidp:blocked_idps:';

    public const DEFAULT_THEME = 'perun';

    public const MFA_PROFILE = 'https://refeds.org/profile/mfa';

    public const URN_CESNET_PROXYIDP_LSIDPENTITYID = 'urn:cesnet:proxyidp:lsidpentityid:';

    // ROOT CONFIGURATION ENTRY
    public const WAYF = 'wayf_config';

    public const INTERFACE = 'interface';

    public const RPC = 'rpc';

    // CONFIGURATION ENTRIES
    public const BOXED = 'boxed';

    public const TRANSLATE_MODULE = 'translate_module';

    public const REMOVE_AUTHN_CONTEXT_CLASS_PREFIXES = 'remove_authn_context_class_ref_prefixes';

    public const PROCESS_AARC_IDP_HINT_ENABLED = 'process_aarc_idp_hint_enabled';

    public const DISPLAY_SP = 'display_sp_name';
    public const DISPLAY_SP_NAME_MODE = 'display_sp_name_mode';
    public const DISPLAY_SP_NAME_MODE_ACR = 'authenticationContextClassReference';
    public const DISPLAY_SP_NAME_MODE_REQUESTER_ID = 'RequesterID';

    public const SKIP_PREVIOUS_SELECTION = 'skip_previous_selection_services';

    public const ONLY_ALLOWED_IDPS = 'only_allowed_idps';

    public const BLOCKED_IDPS = 'blocked_idps';

    // CONFIGURATION ENTRIES IDP BLOCKS
    public const IDP_BLOCKS = 'idp_blocks_config';

    public const IDP_BLOCK_TYPE = 'type';

    public const IDP_BLOCK_TYPE_INLINESEARCH = 'inlinesearch';

    public const IDP_BLOCK_TYPE_TAGGED = 'tagged';

    public const IDP_BLOCK_TEXT_ENABLED = 'text_enabled';

    public const IDP_BLOCK_NAME = 'name';

    public const IDP_BLOCK_HINT_TRANSLATION = 'hint_translation';

    public const IDP_BLOCK_NOTE_TRANSLATION = 'note_translation';

    public const IDP_BLOCK_PLACEHOLDER_TRANSLATE = 'placeholder_translation';

    public const IDP_BLOCK_TAGS = 'tags';

    public const IDP_BLOCK_ENTITY_IDS = 'entity_ids';

    // CONFIGURATION ENTRIES ADD INSTITUTION
    public const ADD_INSTITUTION = 'add_institution_config';

    public const ADD_INSTITUTION_URL = 'url';

    public const ADD_INSTITUTION_EMAIL = 'email';

    // PARAMS AND DATA KEYS
    public const ENTITY_ID = 'entityID';

    public const RETURN = 'return';

    public const RETURN_ID_PARAM = 'returnIDParam';

    public const ORIGINAL_SP = 'originalsp';

    public const IDP_LIST = 'idplist';

    public const PREFERRED_IDP = 'preferredidp';

    public const AUTHN_CONTEXT_CLASS_REF = 'AuthnContextClassRef';

    public const WARNING_ATTRIBUTES = 'warningAttributes';

    public const AUTH_ID = 'AuthID';

    // METADATA KEYS
    public const METADATA_DO_NOT_FILTER_IDPS = 'disco.doNotFilterIdps';

    public const METADATA_ADD_INSTITUTION_APP = 'disco.addInstitutionApp';

    public const METADATA_ONLY_ALLOWED_IDPS = "disco.onlyAllowedIdps";

    public const METADATA_BLOCKED_IDPS = "disco.blockedIdps";

    /** @deprecated */
    public const SP_GREYLIST = 'greylist';

    public const SP_HIDE_FROM_DISCOVERY = 'hide_from_discovery';

    public const IDP_ENTITY_ID = 'entityid';

    public const SP_ENTITY_ID = 'entityid';

    public const IDP_COLOR = 'color';
    public const IDP_TEXT_COLOR = 'textColor';

    public const IDP_FULL_DISPLAY_NAME = 'fullDisplayName';

    public const IDP_SHOW_SIGN_IN_WITH = 'showSignInWith';

    // STATE KEYS
    public const STATE_SP_METADATA = 'SPMetadata';

    public const SAML_REQUESTED_AUTHN_CONTEXT = 'saml:RequestedAuthnContext';

    public const SAML_REQUESTED_AUTHN_CONTEXT_ORIGINAL = 'saml:RequestedAuthnContext:original';

    public const STATE_AUTHN_CONTEXT_CLASS_REF = 'AuthnContextClassRef';

    public const SAML_SP_SSO = 'saml:sp:sso';

    public const NAME = 'name';

    // DISPLAY SERVICE NAME KEYS
    public const ENTITY_ID_PREFIX = 'urn:cesnet:proxyidp:entity_id:';

    public const SERVICE_NAME_ATTR = 'service_name_attr';

    public const SERVICE_NAME_DEFAULT_ATTR_NAME = 'perunFacilityAttr_spname';

    public const CLIENT_ID_ATTR = 'client_id_attr';

    public const ENTITY_ID_ATTR = 'entity_id_attr';

    public const DISCO_IDP_SELECTED_REDIRECT = 'perun/disco_idp_selected.php';

    // VARIABLES

    private $originalsp;

    private array $originalAuthnContextClassRef = [];

    private $wayfConfiguration;

    private $perunModuleConfiguration;

    private $spName;

    private $adapter;

    private $proxyIdpEntityId;
    private $processAarcIdpHintEnabled;

    private $state;

    public function __construct(array $metadataSets, $instance, bool $setUpstreamRequestedAuthnContext)
    {
        //LOAD CONFIG FOR MODULE PERUN, WHICH CONTAINS WAYF CONFIGURATION
        try {
            $this->perunModuleConfiguration = Configuration::getConfig(self::CONFIG_FILE_NAME);
            $this->wayfConfiguration = $this->perunModuleConfiguration->getConfigItem(self::WAYF);
        } catch (\Exception $ex) {
            Logger::error(self::DEBUG_PREFIX . "missing or invalid '" . self::CONFIG_FILE_NAME . "' config file");
            throw $ex;
        }

        $this->processAarcIdpHintEnabled = $this->wayfConfiguration->getBoolean(
            self::PROCESS_AARC_IDP_HINT_ENABLED,
            true
        );

        if (!array_key_exists(self::RETURN, $_GET)) {
            throw new \Exception('Missing parameter: ' . self::RETURN);
        }
        $returnURL = HTTP::checkURLAllowed($_GET[self::RETURN]);

        parse_str(parse_url($returnURL)['query'], $query);

        if (isset($query[self::AUTH_ID])) {
            $id = explode(':', $query[self::AUTH_ID])[0];
            $this->authId = $id;
            $state = State::loadState($id, self::SAML_SP_SSO, true);

            if ($state !== null) {
                if (!empty($state[self::SAML_REQUESTED_AUTHN_CONTEXT][self::AUTHN_CONTEXT_CLASS_REF])) {
                    $this->originalAuthnContextClassRef =
                        $state[self::SAML_REQUESTED_AUTHN_CONTEXT][self::AUTHN_CONTEXT_CLASS_REF];
                    $this->removeAuthContextClassRefWithPrefixes($state);
                }

                if ($setUpstreamRequestedAuthnContext) {
                    DiscoUtils::setUpstreamRequestedAuthnContext($state);
                }

                if (isset($state['IdPMetadata']['entityid'])) {
                    $this->proxyIdpEntityId = $state['IdPMetadata']['entityid'];
                }
                State::saveState($state, self::SAML_SP_SSO);

                $e = explode('=', $returnURL)[0];
                $newReturnURL = $e . '=' . urlencode($id);
                $_GET[self::RETURN] = $newReturnURL;
            }
            $this->state = $state;
        }

        parent::__construct($metadataSets, $instance);

        if (isset($state, $state[self::STATE_SP_METADATA])) {
            $this->originalsp = $state[self::STATE_SP_METADATA];
        }
    }

    /**
     * Handles a request to this discovery service. It is entry point of Discovery service.
     *
     * The IdP disco parameters should be set before calling this function.
     */
    public function handleRequest()
    {
        // test if user has selected an idp or idp can be determined automatically somehow.
        $this->start();

        // no choice possible. Show discovery service page
        $idpList = $this->getIdPList();
        if (
            isset($this->originalsp[self::METADATA_ADD_INSTITUTION_APP])
            && $this->originalsp[self::METADATA_ADD_INSTITUTION_APP] === true
        ) {
            $idpList = $this->filterAddInstitutionList($idpList);
        } else {
            $idpList = $this->filterList($idpList);
        }

        if (sizeof($idpList) === 1) {
            $idp = array_keys($idpList)[0];
            $url = self::buildContinueUrl($this->spEntityId, $this->returnURL, $this->returnIdParam, $idp);
            Logger::info(self::DEBUG_PREFIX . 'Only one Idp left. Redirecting automatically. IdP: ' . $idp);
            HTTP::redirectTrustedURL($url);
        }

        $onlyAllowedIdps = [];
        $blockedIdps = [];
        $hintedIdp = null;
        // IF IS SET AUTHN CONTEXT CLASS REF, REDIRECT USER TO THE IDP
        if (!empty($this->originalAuthnContextClassRef)) {
            // Check authnContextClassRef and select IdP directly if the correct value is set
            foreach ($this->originalAuthnContextClassRef as $acrValue) {
                // VERIFY THE PREFIX IS CORRECT AND WE CAN PERFORM THE REDIRECT
                if (str_starts_with($acrValue, self::URN_CESNET_PROXYIDP_IDPENTITYID)) {
                    $hintedIdp = $this->getHintedIdpViaAcr($acrValue);
                } elseif (str_starts_with($acrValue, self::URN_CESNET_PROXYIDP_ONLY_ALLOWED_IDPS)) {
                    $onlyAllowedIdps = $this->processOnlyAllowedIdpsAcr($acrValue);
                    $onlyAllowedIdps = array_fill_keys($onlyAllowedIdps, 1);
                } elseif (str_starts_with($acrValue, self::URN_CESNET_PROXYIDP_BLOCKED_IPDS)) {
                    $blockedIdps = $this->processBlockedIdpsAcr($acrValue);
                    $blockedIdps = array_fill_keys($blockedIdps, 1);
                }
            }
        }

        if (!empty($this->originalsp[Disco::METADATA_ONLY_ALLOWED_IDPS])) {
            $onlyAllowedIdps = $this->originalsp[Disco::METADATA_ONLY_ALLOWED_IDPS];
            $onlyAllowedIdps = array_fill_keys($onlyAllowedIdps, 1);
        }

        if ($this->processAarcIdpHintEnabled) {
            $hintedIdp = $this->processAarcIdpHint($this->state, $hintedIdp);
        }
        if (!empty($onlyAllowedIdps)) {
            $idpList = array_intersect_key($idpList, $onlyAllowedIdps);
        } elseif (!empty($blockedIdps)) {
            $idpList = array_diff_key($idpList, $blockedIdps);
        }
        if (empty($idpList)) {
            throw new Module\saml\Error\NoAvailableIDP(
                Constants::STATUS_REQUESTER,
                'None of the IdPs requested are available to this proxy.'
            );
        } elseif ($hintedIdp) {
            $this->redirectToHintedIdp($hintedIdp, $idpList);
            // either will throw an exception or redirect user, we can end prematurely
            return;
        } elseif (sizeof($idpList) === 1) {
            Logger::info(
                self::DEBUG_PREFIX .
                'Only one Idp left. Redirecting automatically. IdP: ' .
                array_key_first($idpList)
            );
            $idp = array_keys($idpList)[0];
            self::redirectToIdP($idp);
        }

        $preferredIdP = $this->getPreferredIdp($idpList);
        $warningAttributes = $this->getWarningAttributes();

        $t = new DiscoTemplate($this->config);
        $displaySpName = $this->processSpDisplayName($t);
        $skipPreviousSelection = $this->processSkipPreviousSelection();

        $t->data[self::ORIGINAL_SP] = $this->originalsp;
        $t->data[self::IDP_LIST] = $this->idplistStructured($idpList);
        $t->data[self::PREFERRED_IDP] = $preferredIdP;
        $t->data[self::ENTITY_ID] = $this->spEntityId;
        $t->data[self::RETURN] = $this->returnURL;
        $t->data[self::RETURN_ID_PARAM] = $this->returnIdParam;
        $t->data[self::AUTHN_CONTEXT_CLASS_REF] = $this->originalAuthnContextClassRef;
        $t->data[self::WARNING_ATTRIBUTES] = $warningAttributes;
        $t->data[self::WAYF] = $this->wayfConfiguration;
        $t->data[self::NAME] = $this->spName;
        $t->data[self::DISPLAY_SP] = $displaySpName;
        $t->data[self::SKIP_PREVIOUS_SELECTION] = $skipPreviousSelection;
        $t->show();
    }

    /**
     * Filter out IdP which:
     * 1. are not in SAML2 Scoping attribute list (SAML2 feature)
     * 2. do not support research and scholarship
     * 3. do not support code of conduct.
     *
     * @param array $list a map of entities to filter
     *
     * @return array the list in $list after filtering entities
     */
    public static function doFilter(
        array $list,
        array $scopedIdPList = []
    ): array {
        return self::scoping($list, $scopedIdPList);
    }

    /**
     * @param $entityID
     * @param $return
     * @param $returnIDParam
     * @param $idpEntityId
     *
     * @return string url where user should be redirected when he choose idp
     */
    public static function buildContinueUrl(
        string $entityID,
        string $return,
        string $returnIDParam,
        string $idpEntityId,
        bool $relative = false
    ): string {
        if ($relative) {
            return '?' .
                'entityID=' . urlencode($entityID) . '&' .
                'return=' . urlencode($return) . '&' .
                'returnIDParam=' . urlencode($returnIDParam) . '&' .
                'idpentityid=' . urlencode($idpEntityId);
        }
        return Module::getModuleURL(self::DISCO_IDP_SELECTED_REDIRECT) . '?' .
            'entityID=' . urlencode($entityID) . '&' .
            'return=' . urlencode($return) . '&' .
            'returnIDParam=' . urlencode($returnIDParam) . '&' .
            'idpentityid=' . urlencode($idpEntityId);
    }

    /**
     * @param $entityID
     * @param $return
     * @param $returnIDParam
     *
     * @return string url where user should be redirected when he choose idp
     */
    public static function buildContinueUrlWithoutIdPEntityId(
        string $entityID,
        string $return,
        string $returnIDParam,
        bool $relative = false
    ): string {
        if ($relative) {
            return '?' .
                'entityID=' . urlencode($entityID) . '&' .
                'return=' . urlencode($return) . '&' .
                'returnIDParam=' . urlencode($returnIDParam);
        }
        return Module::getModuleURL(self::DISCO_IDP_SELECTED_REDIRECT) . '?' .
            'entityID=' . urlencode($entityID) . '&' .
            'return=' . urlencode($return) . '&' .
            'returnIDParam=' . urlencode($returnIDParam);
    }

    /**
     * This method remove all AuthnContextClassRef which start with prefix from configuration.
     *
     * @param mixed $state
     */
    public function removeAuthContextClassRefWithPrefixes(&$state)
    {
        $prefixes = $this->wayfConfiguration->getArray(self::REMOVE_AUTHN_CONTEXT_CLASS_PREFIXES, []);

        if (empty($prefixes)) {
            return;
        }
        unset($state[self::SAML_REQUESTED_AUTHN_CONTEXT][self::STATE_AUTHN_CONTEXT_CLASS_REF]);
        $filteredAcrs = [];
        foreach ($this->originalAuthnContextClassRef as $acr) {
            $acr = trim($acr);
            $retain = true;
            foreach ($prefixes as $prefix) {
                if (substr($acr, 0, strlen($prefix)) === $prefix) {
                    $retain = false;
                    break;
                }
            }
            if ($retain) {
                array_push($filteredAcrs, $acr);
            }
        }
        if (!empty($filteredAcrs)) {
            $state[self::SAML_REQUESTED_AUTHN_CONTEXT][self::STATE_AUTHN_CONTEXT_CLASS_REF] = $filteredAcrs;
        }
    }

    /**
     * @param DiscoTemplate $t template
     * @param array $metadata IdP metadata associative array
     * @param bool $favourite flag if IdP is favourite
     *
     * @return string html
     */
    public static function showEntry(
        DiscoTemplate $t,
        array $metadata,
        bool $favourite = false
    ): string {
        $searchData = htmlspecialchars(self::constructSearchData($metadata));
        $extra = ($favourite ? ' favourite' : '');
        $href = $t->getContinueUrl($metadata[self::IDP_ENTITY_ID]);
        $html = '<a class="metaentry' . $extra .
            ' list-group-item" data-search="' . $searchData . '" href="' . $href . '">';
        $html .= '<strong>' . $t->getTranslatedEntityName($metadata) . '</strong>';
        $html .= '</a>';

        return $html;
    }

    /**
     * @return string html
     */
    public static function showTaggedEntry(DiscoTemplate $t, array $metadata, string $class = ''): string
    {
        if (!array_key_exists('tags', $metadata)) {
            return self::showEntry($t, $metadata);
        }
        $bck = '#CACACA';
        if (!empty($metadata[self::IDP_COLOR])) {
            $bck = $metadata[self::IDP_COLOR];
        }

        $textColor = 'white';
        if (!empty($metadata[self::IDP_TEXT_COLOR])) {
            $textColor = $metadata[self::IDP_TEXT_COLOR];
        }

        $href = $t->getContinueUrl($metadata[self::IDP_ENTITY_ID]);
        $text = '';
        if (isset($metadata[self::IDP_FULL_DISPLAY_NAME])) {
            $text = $metadata[self::IDP_FULL_DISPLAY_NAME];
        } elseif (isset($metadata[self::IDP_SHOW_SIGN_IN_WITH]) && $metadata[self::IDP_SHOW_SIGN_IN_WITH]) {
            $text = $t->t('{perun:disco:sign_in_with}') . $t->getTranslatedEntityName($metadata);
        } else {
            $text .= $t->getTranslatedEntityName($metadata);
        }
        $html = '<div class="' . $class . '">' . PHP_EOL;
        $html .= '    <div class="metalist list-group">' . PHP_EOL;
        $html .= '        <a class="metaentry btn btn-block tagged" href="' . $href .
            '" style="background: ' . $bck . '; color: ' . $textColor . ';">';
        if (!empty($metadata['icon'])) {
            $html .= '            <img alt="icon" src="'
                . $metadata['icon'] . '"><strong>' . $text . '</strong>' . PHP_EOL;
        }
        $html .= '        </a>';
        $html .= '    </div>';
        $html .= '</div>';

        return $html;
    }

    public static function getOr($id = null): string
    {
        $or = '';
        if ($id !== null) {
            $or .= '<div class="hrline" id="' . $id . '">' . PHP_EOL;
        } else {
            $or .= '<div class=" hrline">' . PHP_EOL;
        }
        $or .= '    <span>or</span>' . PHP_EOL;
        $or .= '</div>';

        return $or;
    }

    public static function showTaggedIdPs(
        DiscoTemplate $t,
        Configuration $blockConfig
    ): string {
        $html = '';
        $selectedIdps = [];
        $tags = $blockConfig->getArray(self::IDP_BLOCK_TAGS, []);
        foreach ($tags as $tag) {
            $selectedIdps = array_merge($selectedIdps, $t->getIdPs($tag));
        }
        $entityIds = $blockConfig->getArray(self::IDP_BLOCK_ENTITY_IDS, []);
        $allIdps = $t->getAllIdps();
        foreach ($entityIds as $entityId) {
            $selectedIdps[] = $allIdps[$entityId];
        }

        $idpCount = count($selectedIdps);
        if ($idpCount === 0) {
            return $html;
        }
        $html .= '<div class="col-12 row">' . PHP_EOL;
        $html .= self::addLoginOptionHint($t, $blockConfig);

        $counter = 0;
        $fullRows = floor($idpCount / 3);
        $remainingIdps = $idpCount % 3;
        $class = 'col-xs-12 col-md-6 col-lg-4';
        for ($i = 0; $i < $fullRows; ++$i) {
            for ($j = 0; $j < 3; ++$j) {
                if ($remainingIdps === 0 && $counter === ($idpCount - 1)) {
                    $class .= ' col-md-offset-3 col-lg-offset-0';
                }
                $html .= self::showTaggedEntry($t, $selectedIdps[array_keys($selectedIdps)[$counter++]], $class);
            }
        }

        $html .= self::showRemainingTaggedEntries($t, $selectedIdps, $counter, $remainingIdps, $fullRows > 0);
        $html .= self::addLoginOptionNote($t, $blockConfig);
        $html .= '</div>' . PHP_EOL;

        return $html;
    }

    public static function showWarning(DiscoTemplate $t, WarningConfiguration $warningConf): string
    {
        $html = '';
        switch ($warningConf->getType()) {
            case WarningConfiguration::WARNING_TYPE_INFO:
                $html .= '<div class="alert alert-info">' . PHP_EOL;
                break;
            case WarningConfiguration::WARNING_TYPE_WARNING:
                $html .= '<div class="alert alert-warning">' . PHP_EOL;
                break;
            case WarningConfiguration::WARNING_TYPE_ERROR:
                $html .= '<div class="alert alert-danger">' . PHP_EOL;
                break;
        }

        $html .= '<h4><strong>' . $t->t('{perun:disco:warning_title}') . '</strong></h4>' . PHP_EOL;
        $html .= $t->t('{perun:disco:warning_text}');
        $html .= '</div>';

        return $html;
    }

    public static function showInlineSearch(
        DiscoTemplate $t,
        Configuration $blockConfig,
        Configuration $addInstitution = null
    ): string {
        $idpList = $t->getIdPs();
        $result = '';
        $isAddInstitutionApp = $t->isAddInstitutionApp();
        $addInstitutionUrl = '';
        $addInstitutionEmail = '';
        if ($addInstitution !== null) {
            $addInstitutionUrl = $addInstitution->getString(self::ADD_INSTITUTION_URL, '');
            $addInstitutionEmail = $addInstitution->getString(self::ADD_INSTITUTION_EMAIL, '');
        }
        $placeholderTranslateKey = self::getPlaceholderTranslation(
            $t,
            $blockConfig,
            '{perun:disco:institution_search_input_placeholder}'
        );

        $metaListHtml = '';
        foreach ($idpList as $idpentry) {
            $metaListHtml .= (self::showEntry($t, $idpentry) . PHP_EOL);
        }

        $result .= self::addLoginOptionNote($t, $blockConfig, '{perun:disco:institution_search_hint}');
        $result .= '<div id="type-more" class="small text-muted">'
            . $t->t('{perun:disco:search_start_hint}')
            . '</div>'
            . PHP_EOL;
        $result .= '<div class="inlinesearch col-12">' . PHP_EOL;
        $result .= '    <form id="idpselectform" action="?" method="get">' . PHP_EOL;
        $result .= '        <input class="inlinesearch form-control-lg" type="text" value=""
                name="query" id="query" autofocus placeholder="'
            . $t->t($placeholderTranslateKey) . '"/>' . PHP_EOL;
        $result .= '    </form>';
        // ENTRIES
        $result .= '    <div class="metalist list-group" id="list"></div>' . PHP_EOL;
        $result .= '    <div class="metalist list-group d-none" id="list-hidden">' . PHP_EOL;
        $result .= $metaListHtml;
        $result .= '</div>' . PHP_EOL;
        // TOO MUCH ENTRIES BLOCK
        $result .= '    <div id="warning-entries" class="alert alert-info entries-warning-block">' . PHP_EOL;
        $result .= '        '
            . $t->t(
                '{perun:disco:institution_search_display_too_much_entries_header}',
                [
                    '<COUNT_HTML>' => '<span id="results-cnt">0</span>',
                ]
            )
            . PHP_EOL;
        $result .= '        <div class="col">' . PHP_EOL;
        $result .= '            <button class="btn btn-block btn-info" id="warning-entries-btn-force-show">';
        $result .= ' ' . $t->t('{perun:disco:institution_search_display_too_much_entries_btn}') . '</button>'
            . PHP_EOL;
        $result .= '        </div>' . PHP_EOL;
        $result .= '    </div>' . PHP_EOL;
        // NO ENTRIES BLOCK
        $result .= '    <div id="no-entries" class="no-idp-found alert alert-info entries-warning-block">'
            . PHP_EOL;
        if ($isAddInstitutionApp && $addInstitutionEmail !== null) {
            $result .= '        ' . $t->t('{perun:disco:add_institution_no_entries_contact_us}') .
                ' <a href="mailto:' . $addInstitutionEmail . '?subject=Request%20for%20adding%20new%20IdP">' .
                $addInstitutionEmail . '</a>' . PHP_EOL;
        } else {
            $result .= '       ' . $t->t('{perun:disco:institution_search_no_entries_header}');
            if ($addInstitutionUrl !== null) {
                $result .= '<br/><br/>' . PHP_EOL;
                $result .= '       ' .
                    ' <a class="btn btn-info btn-block" href="' . $addInstitutionUrl . '">' .
                    $t->t('{perun:disco:institution_search_no_entries_add_institution_text}') . '</a>' . PHP_EOL;
            }
        }
        $result .= '    </div>' . PHP_EOL;
        $result .= '</div>';
        $result .= self::addLoginOptionNote($t, $blockConfig);

        return $result;
    }

    public static function getScripts(bool $boxed): string
    {
        $html = '<script type="text/javascript" src="' .
            Module::getModuleUrl('perun/res/js/jquery.js') . '"></script>' . PHP_EOL;

        $html .= '<script type="text/javascript" src="' .
            Module::getModuleUrl('perun/res/js/old-browsers.js') . '"></script>' . PHP_EOL;

        $html .= '<script type="text/javascript" src="' .
            Module::getModuleUrl('perun/res/js/disco.js') . '"></script>' . PHP_EOL;
        if ($boxed) {
            $html .= self::boxedDesignScript() . PHP_EOL;
        }

        return $html;
    }

    /**
     * Filter a list of entities according to any filters defined in the parent class, plus.
     *
     * @param array $list a map of entities to filter
     *
     * @return array the list in $list after filtering entities
     */
    protected function filterList($list): array
    {
        if (
            !isset($this->originalsp[self::METADATA_DO_NOT_FILTER_IDPS])
            || !$this->originalsp[self::METADATA_DO_NOT_FILTER_IDPS]
        ) {
            $list = parent::filterList($list);
            $list = self::doFilter($list, $this->scopedIDPList);
            $list = $this->hideFromDiscovery($list, $this->originalsp);
        }

        if (empty($list)) {
            throw new Exception('All IdPs has been filtered out. And no one left.');
        }

        return $list;
    }

    /**
     * Filter out IdP which are not in SAML2 Scoping attribute list (SAML2 feature).
     *
     * @param array $list a map of entities to filter
     *
     * @return array the list in $list after filtering entities
     */
    protected static function scoping(array $list, array $scopedIDPList): array
    {
        if (!empty($scopedIDPList)) {
            if (isset($sp[self::SP_HIDE_FROM_DISCOVERY])) {
                $hideFromDiscovery = $sp[self::SP_HIDE_FROM_DISCOVERY];
            } elseif (isset($sp[self::SP_GREYLIST])) {
                Logger::warning(
                    sprintf(
                        '%sYou are using a deprecated option %s in your SP metadata. Please rename it to %s.',
                        self::DEBUG_PREFIX,
                        self::SP_GREYLIST,
                        self::SP_HIDE_FROM_DISCOVERY
                    )
                );
                $hideFromDiscovery = $sp[self::SP_GREYLIST];
            } else {
                return $list;
            }

            return array_diff_key($list, array_flip($hideFromDiscovery));
        }

        return $list;
    }

    protected function hideFromDiscovery($list, $sp): array
    {
        foreach ($list as $entityId => $idp) {
            if (
                isset($sp[self::SP_GREYLIST])
                && in_array($entityId, $sp[self::SP_GREYLIST], true)
            ) {
                unset($list[$entityId]);
            }
        }

        return $list;
    }

    protected static function showRemainingTaggedEntries(
        DiscoTemplate $t,
        array $idps,
        int $counter,
        int $remainingIdps,
        bool $hasFullRows
    ): string {
        $html = '';
        if ($remainingIdps === 0) {
            return $html;
        }

        if ($hasFullRows > 0) {
            if ($remainingIdps === 2) {
                $class = 'col-xs-12 col-md-6 col-lg-4 col-lg-offset-2';
                $html .= self::showTaggedEntry($t, $idps[array_keys($idps)[$counter++]], $class);
                $class = 'col-xs-12 col-md-6 col-lg-4 col-lg-offset-0 col-md-offset-3';
                $html .= self::showTaggedEntry($t, $idps[array_keys($idps)[$counter]], $class);
            } elseif ($remainingIdps === 1) {
                $class = 'col-xs-12 col-md-6 col-lg-4 col-lg-offset-4';
                $html .= self::showTaggedEntry($t, $idps[array_keys($idps)[$counter]], $class);
                $html .= '</div>' . PHP_EOL;
            }
        } elseif ($remainingIdps === 2) {
            $class = 'col-xs-12 col-md-6';
            $html .= self::showTaggedEntry($t, $idps[array_keys($idps)[$counter++]], $class);
            $html .= self::showTaggedEntry($t, $idps[array_keys($idps)[$counter]], $class);
        } elseif ($remainingIdps === 1) {
            $class = 'col-lg-12';
            $html .= self::showTaggedEntry($t, $idps[array_keys($idps)[$counter]], $class);
        }

        return $html;
    }

    private static function addLoginOptionHint(
        DiscoTemplate $t,
        Configuration $blockConfig,
        string $defaultTranslateKey = ''
    ): string {
        $textOn = $blockConfig->getBoolean(self::IDP_BLOCK_TEXT_ENABLED, false);
        $name = $blockConfig->getString(self::IDP_BLOCK_NAME, '');
        $hintTranslate = $blockConfig->getArray(self::IDP_BLOCK_HINT_TRANSLATION, []);
        $hintTranslateKey = !empty($name) ? '{perun:disco:' . $name . '_hint}' : '';
        if ($textOn && !empty($hintTranslateKey) && !empty($hintTranslate)) {
            $t->includeInlineTranslation($hintTranslateKey, $hintTranslate);

            return '<p class="login-option-category-hint">' . $t->t($hintTranslateKey) . '</p>' . PHP_EOL;
        }
        if ($textOn && !empty($defaultTranslateKey)) {
            return '<p class="login-option-category-hint">' . $t->t($defaultTranslateKey) . '</p>' . PHP_EOL;
        }

        return '';
    }

    private static function addLoginOptionNote(
        DiscoTemplate $t,
        Configuration $blockConfig,
        string $defaultTranslateKey = ''
    ): string {
        $textOn = $blockConfig->getBoolean(self::IDP_BLOCK_TEXT_ENABLED, false);
        $name = $blockConfig->getString(self::IDP_BLOCK_NAME, '');
        $noteTranslate = $blockConfig->getArray(self::IDP_BLOCK_NOTE_TRANSLATION, []);
        $noteTranslateKey = !empty($name) ? '{perun:disco:' . $name . '_note}' : '';
        if ($textOn && !empty($noteTranslateKey) && !empty($noteTranslate)) {
            $t->includeInlineTranslation($noteTranslateKey, $noteTranslate);

            return '<p class="login-option-category-note">' . $t->t($noteTranslateKey) . '</p>' . PHP_EOL;
        }
        if ($textOn && !empty($defaultTranslateKey)) {
            return '<p class="login-option-category-note">' . $t->t($defaultTranslateKey) . '</p>' . PHP_EOL;
        }

        return '';
    }

    private static function getPlaceholderTranslation(
        DiscoTemplate $t,
        Configuration $blockConfig,
        string $translateKey
    ): string {
        $translate = $blockConfig->getArray(self::IDP_BLOCK_PLACEHOLDER_TRANSLATE, []);
        if (!empty($translate)) {
            $t->includeInlineTranslation($translateKey, $translate);
        }

        return $translateKey;
    }

    private static function boxedDesignScript(): string
    {
        $script = '<script>' . PHP_EOL;
        $script .= '   $("#wrap").css("box-shadow", "0 1rem 3rem 0.5rem rgba(0, 0, 0, .15)");' . PHP_EOL;
        $script .= '</script>';

        return $script;
    }

    private static function arrayFlatten($array): array
    {
        $return = [];
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                if (is_array($value)) {
                    $return = [...$return, ...self::arrayFlatten($value)];
                } else {
                    $return = [...$return, $value];
                }
            }
        } else {
            $return = [$array];
        }

        return $return;
    }

    private static function constructSearchData($idpMetadata): string
    {
        $res = '';
        $dataSearchKeys = [];
        if (!empty($idpMetadata['UIInfo'])) {
            $idpMetadata = array_merge($idpMetadata, $idpMetadata['UIInfo']);
        }

        $keys = ['OrganizationName', 'OrganizationDisplayName', 'name', 'scope', 'DisplayName'];

        foreach ($keys as $key) {
            if (!empty($idpMetadata[$key])) {
                $dataSearchKeys = [...$dataSearchKeys, ...self::arrayFlatten($idpMetadata[$key])];
            }
        }
        $dataSearchKeys = array_unique($dataSearchKeys);
        $res .= (' ' . implode(' ', $dataSearchKeys));


        return transliterator_transliterate("Any-Latin; Latin-ASCII; Lower(); [:Punctuation:] Remove", $res);
    }

    private function fillSpName($t, $displaySpNameMode)
    {
        $clientId = null;
        $entityId = null;
        if (
            $displaySpNameMode === self::DISPLAY_SP_NAME_MODE_ACR
            && !empty($this->originalAuthnContextClassRef)
        ) {
            $clientId = CommonUtils::substrInArray(
                PerunConstants::CLIENT_ID_PREFIX,
                $this->originalAuthnContextClassRef
            );
            if (empty($clientId)) {
                $entityId = CommonUtils::substrInArray(self::ENTITY_ID_PREFIX, $this->originalAuthnContextClassRef);
            }
        } elseif (
            $displaySpNameMode === self::DISPLAY_SP_NAME_MODE_REQUESTER_ID
            && !empty($this->state[PerunConstants::REQUESTER_ID])
        ) {
            $clientId = CommonUtils::substrInArray(
                PerunConstants::CLIENT_ID_PREFIX,
                $this->state[PerunConstants::REQUESTER_ID]
            );
            if (empty($clientId) && !empty($this->state[PerunConstants::REQUESTER_ID][0])) {
                $entityId = $this->state[PerunConstants::REQUESTER_ID][0];
            }
        }

        $this->adapter = Adapter::getInstance(
            $this->wayfConfiguration->getString(
                self::INTERFACE,
                self::RPC
            )
        );
        if (!empty($entityId) || !empty($clientId)) {
            try {
                if ($clientId !== null) {
                    $this->fillSpNameForOidc($t, $clientId);
                } elseif ($entityId !== null) {
                    $this->fillSpNameForSaml($t, $entityId);
                }
            } catch (\Exception $e) {
                Logger::debug(self::DEBUG_PREFIX . "Fill SP name - caught exception {$e}");
                //OK, we will just display the disco
            }
        } else {
            $this->fillSpNameForSaml($t, null);
        }
    }

    private function fillSpNameForOidc($t, $clientIdWithPrefix)
    {
        if ($clientIdWithPrefix === null) {
            return;
        }
        $clientId = str_replace(PerunConstants::CLIENT_ID_PREFIX, '', $clientIdWithPrefix);

        $clientIdAttr = $this->wayfConfiguration->getString(self::CLIENT_ID_ATTR, null);
        if ($clientIdAttr === null) {
            $facility = $this->adapter->getFacilityByClientId($clientId);
        } else {
            $facility = $this->adapter->getFacilityByClientId($clientId, $clientIdAttr);
        }

        if ($facility !== null) {
            $spNameAttrName = $this->wayfConfiguration->getString(
                self::SERVICE_NAME_ATTR,
                self::SERVICE_NAME_DEFAULT_ATTR_NAME
            );
            $spNameMap = $this->adapter->getFacilityAttribute($facility, $spNameAttrName);
            if (!empty($spNameMap)) {
                $this->spName = $t->getTranslation($spNameMap);
            }
        }
    }

    private function fillSpNameForSaml($t, $entityIdWithPrefix)
    {
        if ($entityIdWithPrefix === null) {
            $this->spName = null;
            if (!empty($this->originalsp[self::SP_ENTITY_ID])) {
                $entityId = $this->originalsp[self::SP_ENTITY_ID];
                $entityIdAttr = $this->wayfConfiguration->getString(self::ENTITY_ID_ATTR, null);
                if ($entityIdAttr === null) {
                    $facility = $this->adapter->getFacilityByEntityId($entityId);
                } else {
                    $facility = $this->adapter->getFacilityByEntityId($entityId, $entityIdAttr);
                }

                if ($facility === null) {
                    return;
                }

                $spNameAttr = $this->wayfConfiguration->getString(
                    self::SERVICE_NAME_ATTR,
                    self::SERVICE_NAME_DEFAULT_ATTR_NAME
                );

                $spNameMap = $this->adapter->getFacilityAttribute($facility, $spNameAttr);
                if (!empty($spNameMap)) {
                    $this->spName = $t->getTranslation($spNameMap);
                }
            }
            if (empty($this->spName) && !empty($this->originalsp[self::NAME])) {
                $this->spName = $t->translate->getTranslation($this->originalsp[self::NAME]);
            }
        } else {
            $entityId = str_replace(self::ENTITY_ID_PREFIX, '', $entityIdWithPrefix);

            $entityIdAttr = $this->wayfConfiguration->getString(self::ENTITY_ID_ATTR, null);
            if ($entityIdAttr === null) {
                $facility = $this->adapter->getFacilityByEntityId($entityId);
            } else {
                $facility = $this->adapter->getFacilityByEntityId($entityId, $entityIdAttr);
            }

            if ($facility !== null) {
                $spNameAttrName = $this->wayfConfiguration->getString(
                    self::SERVICE_NAME_ATTR,
                    self::SERVICE_NAME_DEFAULT_ATTR_NAME
                );
                $spNameMap = $this->adapter->getFacilityAttribute($facility, $spNameAttrName);
                if (!empty($spNameMap)) {
                    $this->spName = $t->getTranslation($spNameMap);
                }
            }
        }
    }

    private function getSpIdentifier()
    {
        $clientIdWithPrefix = CommonUtils::substrInArray(
            PerunConstants::CLIENT_ID_PREFIX,
            $this->originalAuthnContextClassRef
        );

        if ($clientIdWithPrefix !== null) {
            $parts = explode(':', $clientIdWithPrefix);

            return end($parts); // clientId
        }

        return empty($this->originalsp[self::SP_ENTITY_ID]) ? null : $this->originalsp[self::SP_ENTITY_ID];
    }

    private function processOnlyAllowedIdpsAcr($acrValue)
    {
        $onlyAllowedIdpsStr = substr(
            $acrValue,
            strlen(self::URN_CESNET_PROXYIDP_ONLY_ALLOWED_IDPS),
            strlen($acrValue)
        );
        return explode(";", $onlyAllowedIdpsStr);
    }

    private function processBlockedIdpsAcr($acrValue)
    {
        $blockedIdpsStr = substr(
            $acrValue,
            strlen(self::URN_CESNET_PROXYIDP_BLOCKED_IPDS),
            strlen($acrValue)
        );
        return explode(";", $blockedIdpsStr);
    }

    private function getHintedIdpViaAcr($acrValue)
    {
        $idpEntityId = substr(
            $acrValue,
            strlen(self::URN_CESNET_PROXYIDP_IDPENTITYID),
            strlen($acrValue)
        );
        if ($idpEntityId === $this->proxyIdpEntityId) {
            return null;
        }
        Logger::debug(self::DEBUG_PREFIX . "Resolved IdP EntityID hint via ACR: " . $idpEntityId);
        return $idpEntityId;
    }

    private function getWarningAttributes(): ?WarningConfiguration
    {
        try {
            $warningInstance = WarningConfiguration::getInstance();
            $warningAttributes = $warningInstance->getWarningAttributes();
        } catch (Exception $ex) {
            $warningAttributes = null;
        }
        return $warningAttributes;
    }

    private function getPreferredIdp(array $idpList): ?string
    {
        $preferredIdP = $this->getRecommendedIdP();
        return (!empty($preferredIdP) && array_key_exists($preferredIdP, $idpList)) ? $preferredIdP : null;
    }

    private function processSpDisplayName(DiscoTemplate $t): bool
    {
        $displaySpName = $this->wayfConfiguration->getBoolean(self::DISPLAY_SP, false);
        if ($displaySpName) {
            $displaySpNameMode = $this->wayfConfiguration->getValueValidate(
                self::DISPLAY_SP_NAME_MODE,
                [self::DISPLAY_SP_NAME_MODE_ACR, self::DISPLAY_SP_NAME_MODE_REQUESTER_ID],
                self::DISPLAY_SP_NAME_MODE_ACR
            );
            $this->fillSpName($t, $displaySpNameMode);
        }
        return $displaySpName;
    }

    private function processSkipPreviousSelection(): bool
    {
        $spsToSkipPreviousSelection = $this->wayfConfiguration->getArray(self::SKIP_PREVIOUS_SELECTION, []);
        $spIdentifier = $this->getSpIdentifier();

        return in_array($spIdentifier, $spsToSkipPreviousSelection, true);
    }

    private function redirectToHintedIdp($hintedIdp, $idpList)
    {

        if (!in_array($hintedIdp, $idpList) && !array_key_exists($hintedIdp, $idpList)) {
            Logger::debug(
                self::DEBUG_PREFIX
                . "Hinted IdP '"
                . $hintedIdp
                . " is not available in the IdP list, throwing exception"
            );
            throw new Module\saml\Error\NoAvailableIDP(
                Constants::STATUS_REQUESTER,
                'None of the IdPs requested are available to this proxy.'
            );
        } else {
            Logger::info(self::DEBUG_PREFIX . 'Redirecting to hinted IdP. Redirecting to: ' . $hintedIdp);
            self::redirectToIdp($hintedIdp);
        }
    }

    private function redirectToIdP($idp)
    {
        $url = self::buildContinueUrl($this->spEntityId, $this->returnURL, $this->returnIdParam, $idp);
        Logger::info(self::DEBUG_PREFIX . 'Only one Idp left. Redirecting automatically. IdP: ' . $idp);
        HTTP::redirectTrustedURL($url);
    }

    private function processAarcIdpHint(&$state, $hintedIdp)
    {
        $aarcIdpHint = $state['aarc_idp_hint'] ?? null;
        if (is_null($aarcIdpHint) || empty(trim($aarcIdpHint))) {
            return $hintedIdp;
        }
        $hint = urldecode($aarcIdpHint);
        $parts = explode('?', $hint);
        $aarcHintedIdp = $parts[0];
        if (sizeof($parts) > 1) {
            $state['aarc_idp_hint'] = $parts[1];
        }
        Logger::debug(self::DEBUG_PREFIX . "Resolved IdP EntityID hint via aarc_idp_hint: " . $aarcHintedIdp);
        if (!empty($hintedIdp)) {
            Logger::debug(
                self::DEBUG_PREFIX
                . "IdP hint has been already resolved via other means, ignoring AARC IdP Hint ("
                . $aarcHintedIdp
                . ")"
            );
            return $hintedIdp;
        } else {
            return $aarcHintedIdp;
        }
    }
}
