<?php

declare(strict_types=1);

namespace SimpleSAML\Module\perun;

use SimpleSAML\Utils\Random;

class Consent
{
    private const EU_EAA = [
        'AUT' => 'Austria',
        'BEL' => 'Belgium',
        'BGR' => 'Bulgaria',
        'HRV' => 'Croatia',
        'CYP' => 'Cyprus',
        'CZE' => 'Czech Republic',
        'DNK' => 'Denmark',
        'EST' => 'Estonia',
        'FIN' => 'Finland',
        'FRA' => 'France',
        'DEU' => 'Germany',
        'GRC' => 'Greece',
        'HUN' => 'Hungary',
        'IRL' => 'Ireland',
        'ITA' => 'Italy',
        'LVA' => 'Latvia',
        'LTU' => 'Lithuania',
        'LUX' => 'Luxembourg',
        'MLT' => 'Malta',
        'NLD' => 'Netherlands',
        'PRT' => 'Portugal',
        'ROU' => 'Romania',
        'SVK' => 'Slovakia',
        'SVN' => 'Slovenia',
        'ESP' => 'Spain',
        'SWE' => 'Sweden',
        'NOR' => 'Norway',
        'ISL' => 'Iceland',
        'LIE' => 'Liechtenstein',
        'GBR' => 'United Kingdom',
    ];

    private const JURISDICTION_INTERNATIONAL_ORG = 'International organisation';
    private const JURISDICTION_EMBL = 'EMBL intergovernmental organisation';
    private const JURISDICTION_ERIC = '(ERIC) European Research Infrastructure Consortium';

    public static function perunPresentAttributes($t, $attributes, $nameParent, $labelCol = 5)
    {
        $translator = $t->getTranslator();

        if (strlen($nameParent) > 0) {
            $parentStr = strtolower($nameParent) . '_';
            $str = '<ul class="perun-attributes">';
        } else {
            $parentStr = '';
            $str = '<ul id="perun-table_with_attributes" class="perun-attributes">';
        }

        foreach ($attributes as $name => $value) {
            $nameraw = $name;
            $name = $translator->getAttributeTranslation($parentStr . $nameraw);

            if (preg_match('/^child_/', $nameraw)) {
                // insert child table
                throw new Exception('Unsupported');
            }
            // insert values directly
            $str .= "\n" . '<li>'
                        . '<div class="row"><div class="col-sm-' . $labelCol
                        . '"><h2 class="perun-attrname h4">'
                        . htmlspecialchars(str_replace('domovksé', 'domovské', $name)) . '</h2></div>';

            $str .= '<div class="perun-attrcontainer col-sm-' . (12 - $labelCol) . '">';
            $isHidden = in_array($nameraw, $t->data['hiddenAttributes'], true);
            if ($isHidden) {
                $hiddenId = Random::generateID();
                $str .= '<span class="perun-attrvalue hidden" id="hidden_' . $hiddenId . '">';
            } else {
                $str .= '<span class="perun-attrvalue">';
            }

            if (count($value) > 0) {
                $str .= '<ul class="perun-attrlist">';
                foreach ($value as $listitem) {
                    $str .= '<li>' . self::presentAttributesPhotoOrValue($nameraw, $listitem) . '</li>';
                }
                $str .= '</ul>';
            }
            $str .= '</span>';

            if ($isHidden) {
                $str .= '<div class="perun-attrvalue consent_showattribute" id="visible_' . $hiddenId . '">';
                $str .= '&#8230; ';
                $str .= '<a class="consent_showattributelink" href="javascript:SimpleSAML_show(\'hidden_';
                $str .= $hiddenId;
                $str .= '\'); SimpleSAML_hide(\'visible_' . $hiddenId . '\');">';
                $str .= $t->t('{consent:consent:show_attribute}');
                $str .= '</a>';
                $str .= '</div>';
            }

            $str .= '</div><!-- .perun-attrcontainer --></div><!-- .row --></li>';
            // end else: not child table
        }   // end foreach
        $str .= isset($attributes) ? '</ul>' : '';

        return $str;
    }

    public static function presentAttributesPhotoOrValue($nameraw, $listitem)
    {
        if ($nameraw === 'jpegPhoto') {
            return '<img src="data:image/jpeg;base64,' . htmlspecialchars($listitem) . '" alt="User photo" />';
        }

        return htmlspecialchars($listitem);
    }

    public static function getJurisdiction($dstMetadata): string
    {
        $jurisdiction = empty($dstMetadata['jurisdiction']) ? '' : $dstMetadata['jurisdiction'];
        $pattern = "/\(\K[A-Z]{3}/";
        $jurisdictionCode = preg_match($pattern, $jurisdiction, $out) ? $out[0] : $jurisdiction;
        if (
            self::JURISDICTION_INTERNATIONAL_ORG === $jurisdictionCode
            || self::JURISDICTION_EMBL === $jurisdictionCode
        ) {
            return $jurisdiction;
        } elseif (
            empty($jurisdictionCode)
            || $jurisdiction === self::JURISDICTION_ERIC
            || array_key_exists($jurisdictionCode, self::EU_EAA)
        ) {
            return '';
        }

        return $jurisdiction;
    }

    public static function getJurisdictionDialogue(string $parsedJurisdiction): string
    {
        $html = "";
        if (!empty($parsedJurisdiction)) {
            $html .= '<div class="alert alert-warning text-justify" role="alert">' . PHP_EOL;
            if (
                $parsedJurisdiction === self::JURISDICTION_EMBL
                || $parsedJurisdiction === self::JURISDICTION_INTERNATIONAL_ORG
            ) {
                $html .= '    <h4>This service is provided by an international organization.</h4>' . PHP_EOL;
            } else {
                $html .= '    <h4>This service is in ' . $parsedJurisdiction . '</h4>' . PHP_EOL;
            }
            if ($parsedJurisdiction === self::JURISDICTION_EMBL) {
                $html .= '    <p>In order to access the requested services, the Life Science Login needs to transfer ' .
                    'your personal data to an international organization outside EU/EEA jurisdictions.</p>' .
                    '<p>Please be aware that upon transfer your personal data will be protected by <a href="' .
                    'https://www.embl.org/documents/document/internal-policy-no-68-on-general-data-protection/"' .
                    ' target="_blank">EMBL’s Internal Policy 68 on General Data Protection</a>.</p>' . PHP_EOL;
            } else {
                $html .= '    <p>In order to access the requested services, the Life Science Login needs to transfer ' .
                    'your personal data to a country outside EU/EEA. We cannot guarantee that this country offers ' .
                    'an adequately high level of personal data protection as EU/EEA countries.</p>' . PHP_EOL;
            }
            $html .=  '</div>' . PHP_EOL;
        }
        return $html;
    }

    public static function getPrivacyPolicyDialogueLsaai($spPrivacyPolicy, $dstMetadata): string
    {
        $html = "";
        if (!empty($spPrivacyPolicy)) {
            $html .= '<p>Document with the privacy policy for this service can be found ' .
                    '<a target="_blank" href="' . htmlspecialchars($spPrivacyPolicy) . '">here</a>.</p>';
        } else {
            $html .= '<div class="alert alert-warning" role="alert">' . PHP_EOL .
                '    <h4>Missing Privacy Policy document.</h4>' . PHP_EOL .
                '    <p class="text-justify">This servise has not yet provided link to the Privacy Policy document.';
            if (isset($dstMetadata['test.sp']) && $dstMetadata['test.sp']) {
                $html .= ' This might be due to the service being registered in the test environment, which does not ' .
                    'force the service to do so. To get more information about the different environments of the LS ' .
                    'Login , please visit ' .
                    '<a href="https://lifescience-ri.eu/ls-login/relying-parties/environments.html"' .
                    ' target="_blank">this page</a>.';
            }
            $html .= '</p>' . PHP_EOL;
            $html .= '</div>' . PHP_EOL;
        }
        return $html;
    }

    public static function getAcceptedTosDialogueLsaai($dstMetadata): string
    {
        $html = "";
        if (empty($dstMetadata['accepted_tos'])) {
            $html = '<div class="alert alert-warning" role="alert">' .
                '    <h4>Terms of Use for Service Providers not accepted</h4>' .
                '    <p class="text-justify">You are entering a service which has not yet accepted the' .
                ' <a href="https://lifescience-ri.eu/ls-login/terms-of-use-for-service-providers.html"' .
                ' target="_blank">Terms of Use for Service Providers</a>.';
            if (! isset($dstMetadata['test.sp']) || $dstMetadata['test.sp']) {
                $html .= ' This might be due to the service being registered in the test environment, which does not ' .
                    'force the service to do so. To get more information about the different environments of the LS ' .
                    'Login , please visit ' .
                    '<a href="https://lifescience-ri.eu/ls-login/relying-parties/environments.html"' .
                    ' target="_blank">this page</a>.';
            }
            $html .= '</p></div>' . PHP_EOL;
        }
        return $html;
    }
}
