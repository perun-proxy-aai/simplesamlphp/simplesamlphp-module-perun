# [10.17.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.16.1...v10.17.0) (2025-02-13)


### Features

* only allowed idps Disco support for SAML (needed for Beyond) ([a21a46a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/a21a46a5429c0bcdf2e320f021895930543c70d5))

## [10.16.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.16.0...v10.16.1) (2025-02-06)


### Bug Fixes

*  fix Disco.php to allow relative redirect ([ccd460e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/ccd460ee8201973d43c53819ebd8e3dca2cd8abf))

# [10.16.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.15.0...v10.16.0) (2024-12-29)


### Features

* add theme for elter ([59dcd2b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/59dcd2bb9a73b527b34d0d53c01b78d984b56b71))

# [10.15.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.14.0...v10.15.0) (2024-12-11)


### Features

* ensureVoAup filter ([50a145f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/50a145fe85e7a22bf3b9e312d5cc526d047657ee))
* ensureVoMember can now get facility by clientId in RequseterID and does callback ([d7b43db](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/d7b43db2c288f45b1de53bfbe063e5ea11e81669))

# [10.14.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.13.1...v10.14.0) (2024-12-11)


### Features

* ensureVoMember can now get facility by clientId in RequseterID and does callback ([f1e857a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/f1e857aa659ab46c2e7c5e7f8f249f0a70314206))

## [10.13.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.13.0...v10.13.1) (2024-10-18)


### Bug Fixes

* auth-event-logging-tables correct column names ([000c009](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/000c009c28523bae62e9aba2b49399c3f3a04317))

# [10.13.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.12.3...v10.13.0) (2024-10-18)


### Features

* fixed invalid data inserting in auth_event_filter ([fac3a81](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/fac3a8195b99f7bf9ef1dfa8c8121950aa521d98))

## [10.12.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.12.2...v10.12.3) (2024-10-10)


### Bug Fixes

* requestedAuthnContext from service overrided by idp requested AuthnContext in disco ([3b84585](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/3b84585c31e8fd0d221aa43b36b32a365df9bec4))

## [10.12.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.12.1...v10.12.2) (2024-07-26)


### Bug Fixes

* update composer.json to make compatible with authoauth2 ([56e90a9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/56e90a9a25cf7d00f067b5d973fdad1c7965d0a6))

## [10.12.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.12.0...v10.12.1) (2024-07-17)


### Bug Fixes

* 🐛 Overlapping footer in LS Hostel templates, unify with LS ([128d22c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/128d22c8065f63d1d9cf5f9b108f29aa44709884))

# [10.12.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.11.8...v10.12.0) (2024-07-17)


### Features

* aup manager auth process filter ([e20fe68](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/e20fe6821ec5e0749422d3ede138d6171c565188))

## [10.11.8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.11.7...v10.11.8) (2024-07-11)


### Bug Fixes

* missing const in perun_entitlement_extended ([8da2dca](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/8da2dcafbd6cdb4ac4187997b848d5555a6c30db))

## [10.11.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.11.6...v10.11.7) (2024-07-11)


### Bug Fixes

* fix import in PerunEntitlementExtended ([2e27b61](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/2e27b617411485cff23b829f094ed9cfa7553a22))

## [10.11.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.11.5...v10.11.6) (2024-06-10)


### Bug Fixes

* merge entitlements ([4f83612](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/4f836128430b77d30ea6e2890753c00266fb498d))
* merge entitlements ([a4d2a2d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/a4d2a2d402739d68a9f4caa7dd80abc193fef358))

## [10.11.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.11.4...v10.11.5) (2024-06-07)


### Bug Fixes

* use unfiltered groups for capabilities ([db02494](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/db024945138bb02f458aefed242ded31b1305d63))

## [10.11.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.11.3...v10.11.4) (2024-06-07)


### Bug Fixes

* metadata generation ([2fd8359](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/2fd8359b1cd7f91a94620e00b40afcab45e6e8f0))
* pass groups to getCapabilities ([11bcda7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/11bcda7c76246d1c91ddf7997f130ef6357af8a4))

## [10.11.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.11.2...v10.11.3) (2024-06-07)


### Bug Fixes

* 🐛 use `json_encode` instead of `implode` in updateUes ([9b63237](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/9b6323720ce7f7d1c38870db12049d91be915219))

## [10.11.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.11.1...v10.11.2) (2024-06-06)


### Bug Fixes

* 🐛 Ensure member template ([bb29481](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/bb29481c611cc7406f93d6cac8c9f90c82f0fb47))

## [10.11.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.11.0...v10.11.1) (2024-06-06)


### Bug Fixes

* use another template for PerunIdentity:unauthorize ([23edc64](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/23edc64aabc08c54e631b496af2d93fdda28b921))

# [10.11.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.10.1...v10.11.0) (2024-06-03)


### Features

* entityId override in ProxyFilter ([b1482c4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/b1482c422ea41d19e757c67b2cbe1a497699a26b))

## [10.10.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.10.0...v10.10.1) (2024-05-30)


### Bug Fixes

* correct false handling in getLocalMFAStatus ([6a56ec0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/6a56ec067269087051e823bc99fd61cdfc02fdf3))

# [10.10.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.9.1...v10.10.0) (2024-05-27)


### Features

* local mfa logging ([79ab68c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/79ab68c7956e43660d710aa66cc69f5ae18bd784))

## [10.9.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.9.0...v10.9.1) (2024-05-23)


### Bug Fixes

* 🐛 Add missing import to UpdateUserExtSource AuthProc filt ([b4d9369](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/b4d9369954482075325f06457498c18c422e0fcc))

# [10.9.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.8.0...v10.9.0) (2024-05-15)


### Bug Fixes

* 🐛 Dictionaries - separate definition and translation files ([dac438d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/dac438d90bc0288cade46c837fea1ee3e085ec4f))
* 🐛 Possible call `getId()` on null object in PerunIdentity ([e7e4d4e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/e7e4d4e3182f6d77d088eb25ae95542d1fcd2fd8))


### Features

* 🎸 merge attribute_check into this module ([5fa046b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/5fa046b1dff3d5e01f7240ee697a824f02994c8f))
* 🎸 Merge LSHostel module ([73861f3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/73861f3f922284bdbdec2c945c3b590b15190f3c))

# [10.8.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.7.0...v10.8.0) (2024-05-15)


### Bug Fixes

* apply filtering only to forwarded entitlements ([c5fe4a5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/c5fe4a5b7ee9bc728134acd5c1dca2e064f86a87))


### Features

* groupEntitlementDisabled, proxyAccessControlDisabled ([6062235](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/6062235057ca65f01d0416dcbfc672ae43af3d4a))

# [10.7.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.6.0...v10.7.0) (2024-05-15)


### Features

* entitlements filtering ([6076721](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/6076721d8456b2879aaab26f8b421a919edf3752))

# [10.6.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.5.4...v10.6.0) (2024-05-14)


### Features

* improve contact handling in SpAuthorization ([ecb8719](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/ecb87197d6a5e241b0688befa0c730d229b60321))

## [10.5.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.5.3...v10.5.4) (2024-05-13)


### Bug Fixes

* correct IsBanned, quoting ([9d5c014](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/9d5c0145eee9bc77986943a191a5b6a5ee6a8c27))

## [10.5.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.5.2...v10.5.3) (2024-05-11)


### Bug Fixes

* 🐛 formatting in sprintf in PerunAup ([63eef08](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/63eef0884e7f48e3ca02304f591b25d8a8ecb407))

## [10.5.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.5.1...v10.5.2) (2024-05-10)


### Bug Fixes

* add missing space to ban DB command ([e1917a4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/e1917a4ef106c1ca32c4b03ffe1b3ea1cd0b1cbe))

## [10.5.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.5.0...v10.5.1) (2024-05-10)


### Bug Fixes

* 🐛 unify AUIDs generation with GEANT ([ffcc457](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/ffcc45772e7444e918b0d3b6bf0de3655151864e))

# [10.5.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.4.4...v10.5.0) (2024-05-09)


### Bug Fixes

* 🐛 checking hinted IdP is in the IdP list ([43e889d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/43e889d5aa3178a31df21b3404a033a8490032ce))
* 🐛 update UES helper appendOnly in complex type ([1745895](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/174589543d947184cb01a1657d5b3d973c9dcea9))


### Features

* addditionalIdentifiers auth proc filter ([84c1183](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/84c1183fe26c0c13b26c26cd75c53e47b910c4e5))

## [10.4.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.4.3...v10.4.4) (2024-05-07)


### Bug Fixes

* load groups in user only mode ([a95f952](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/a95f952ea19db7c4bc2b66e33fad66bd327d7169))

## [10.4.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.4.2...v10.4.3) (2024-05-06)


### Bug Fixes

* default value in eduPersonEntitlement ([e13b990](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/e13b9903e96821568f10abae51fa7cf4bb878cae))

## [10.4.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.4.1...v10.4.2) (2024-05-06)


### Bug Fixes

* 🐛 Calling getId on String EntityID ([d091292](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/d091292349706414c1838f47dcf22b2e2bb490bf))

## [10.4.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.4.0...v10.4.1) (2024-05-06)


### Bug Fixes

* define constants in PerunAffiliation ([530939c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/530939c80cf7debff68e6978fadbbda135a25710))

# [10.4.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.3.1...v10.4.0) (2024-05-03)


### Features

* filter for generation affiliations from groups (capabilities) ([e5b8a1d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/e5b8a1df098d941ec2a5658f15626e8290d3f49c))

## [10.3.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.3.0...v10.3.1) (2024-05-03)


### Bug Fixes

* create member only if missing ([c0b7202](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/c0b7202baa02460f64f683df09698244c65b2377))

# [10.3.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.2.0...v10.3.0) (2024-05-03)


### Features

* user_id to auth event logging db table ([fb17202](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/fb17202b6c9abc75c2ece4e5a59374e5597b7ed8))

# [10.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.1.2...v10.2.0) (2024-05-02)


### Features

* extend filters logging ([d0fa4a7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/d0fa4a759836ec0fe24df049532c8ca000c53b63))

## [10.1.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.1.1...v10.1.2) (2024-04-25)


### Bug Fixes

* fix bug from 41421d71 ([d3f0031](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/d3f0031ecda8d94c44553e8e5b3f9d2d614d8bb5))

## [10.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.1.0...v10.1.1) (2024-04-24)


### Bug Fixes

* lower logger level for empty user groups in capabilities ([287d6b8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/287d6b8a4cda4d20a5a094499b71df4ad50ad6ec))

# [10.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v10.0.0...v10.1.0) (2024-04-24)


### Bug Fixes

* 🐛 getRequests in redirecting templates and passing params ([60afda1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/60afda101f59f9c337331e058d0f931a241a607e))
* 🐛 ls template disco link colors ([e1d9932](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/e1d9932dbff3104f8592a8746ef7a97aa0a1af0e))
* 🐛 lsaai.css ([f0d8ff3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/f0d8ff3fb5bdc970de99eca9c6249c375b2493f3))
* 🐛 prevent calling getId() on null user in PerunIdentity ([41421d7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/41421d719c2c9706bd148d9ba4c06f82de27657a))
* 🐛 update LS AAI CSS ([17ab04f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/17ab04f9befd0f5ea71bd493e8330454a8263604))
* 🐛 updateUes filter out empty arrays ([632f719](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/632f71939e8a1b59cfe3737cb40e7686e34ce0ab))


### Features

* 🎸 AuthProc filter AddAttributeValueBasedOnVoMember ([298c2f2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/298c2f2d3494ee07cdabf0cb4d4127980a96c325))
* 🎸 use RequesterID for passing clientID/entityID ([2ffe39f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/2ffe39fc5d798ee73fc637430153b00aa8cb743e))

# [10.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.15.0...v10.0.0) (2024-04-23)


### Features

* is_banned mongo to postgres ([c21eccf](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/c21eccf4373d99a73a907f036ed07feb506e2219))


### BREAKING CHANGES

* is_banned requires postgres instead of mongodb

# [9.15.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.14.2...v9.15.0) (2024-04-22)


### Features

* saml:NameID to session_indexes_detail ([7a8411c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/7a8411c990a442dfa6e5a14bca08e3d6c6a986da))

## [9.14.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.14.1...v9.14.2) (2024-04-19)


### Bug Fixes

* move geodb_reader from constructor to process function ([48a34d5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/48a34d5fed0f818aff19193a08bd31f061249553))
* user agent info from phpuseragentparser library ([ccb0136](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/ccb0136fe5e919f18e58df419919febd9c39be5e))

## [9.14.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.14.0...v9.14.1) (2024-04-17)


### Bug Fixes

* auth_event_filter code fixes ([d58f306](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/d58f3061566790739fe4925392b6b7ce923d1999))

# [9.14.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.13.1...v9.14.0) (2024-04-10)


### Features

* gzip compression in ExtendedSQLStore ([5218e4c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/5218e4c376587254f8b98488f7efcf7d354a90e0))

## [9.13.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.13.0...v9.13.1) (2024-04-10)


### Bug Fixes

* use eppn in session deletion ([c356880](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/c356880555712ed53695ed6e8ae6f19dd3098be8))

# [9.13.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.12.2...v9.13.0) (2024-04-10)


### Features

* auth_event_logging process filter ([5fa58de](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/5fa58dec751a7fbaa02f10c6b505cbf858d3793e))

## [9.12.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.12.1...v9.12.2) (2024-04-10)


### Bug Fixes

* 🐛 btn text color in LS AAI ([b0b6c8d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/b0b6c8d44b04fb404d97d5d0b6f456d0ce7424a9))

## [9.12.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.12.0...v9.12.1) (2024-04-08)


### Bug Fixes

* 🐛 Small fixes in LS AAI CSS ([3afcc15](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/3afcc158ce00e438a03f6be5b44c9b94b8ca2f47))

# [9.12.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.11.0...v9.12.0) (2024-04-08)


### Features

* 🎸 ERIC jurisdiction in consent ([fcabcd7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/fcabcd7a7933d9888f2a402066c942f43ac193ed))

# [9.11.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.10.6...v9.11.0) (2024-03-28)


### Features

* 🎸 Allow using raw user IDs lookup in AUIDs ([f03ebc5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/f03ebc57d58f95fac054db050ed1897b4bbf5770))

## [9.10.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.10.5...v9.10.6) (2024-03-28)


### Bug Fixes

* do not quote array values in ExtendedSQLStore ([5eafca9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/5eafca9980d8c69da2fd885d4c145562071bdbcd))

## [9.10.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.10.4...v9.10.5) (2024-03-26)


### Bug Fixes

* curly braces around array in ExtendedSQLStore ([2b75988](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/2b759882ed99e9cf7ad432730e9ac6c20a451424))

## [9.10.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.10.3...v9.10.4) (2024-03-26)


### Bug Fixes

* replace Perun user ID with EPPN in ExtendedSQLStore ([fbdecdf](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/fbdecdf385f20500dc70f35aa9625e284f7c243f))

## [9.10.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.10.2...v9.10.3) (2024-03-26)


### Bug Fixes

* bugfixes in ExtendedSQLStore ([0171549](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/01715495a20a31776ceb327513332d0a4cf46815))

## [9.10.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.10.1...v9.10.2) (2024-03-22)


### Bug Fixes

* make SQL store more like original ([8ccf6f8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/8ccf6f8fad60b7c8b051aed1c6607d02931c82d8))

## [9.10.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.10.0...v9.10.1) (2024-03-22)


### Bug Fixes

* missing imports ([c75d08d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/c75d08d6cb002bb21251f8f178cf8406f4c61504))

# [9.10.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.9.0...v9.10.0) (2024-03-22)


### Features

* extended SQL store ([48cdefd](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/48cdefd23d53fb2a7e563b31e75af381ad5d22bb))

# [9.9.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.8.0...v9.9.0) (2024-03-22)


### Features

* save perunUserId into attributes ([7b3227d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/7b3227de5957fb4c62315859ee46ae0dad6a28aa))

# [9.8.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.7.3...v9.8.0) (2024-03-22)


### Bug Fixes

* 🐛 visuals - buttons on redirects, disco IdP text color ([8de4415](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/8de4415b4946285a06a9bf1ef2c99eadd970a1b6))


### Features

* 🎸 Blocked and OnlyAllowed IdPs on Disco on per-sp basis ([acc98a1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/acc98a18bdc2edad755b24a33648e5be55f05c44))
* 🎸 LS AAI theme and consent improvements ([d91fe44](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/d91fe44ae8169f66791996b5d83f79772b2d5625))
* 🎸 Pass blocked and only allowed IdPs via ACR from SP ([83fb7ac](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/83fb7ac6b6be59237f088234ea9707571428d925))

## [9.7.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.7.2...v9.7.3) (2024-03-18)


### Bug Fixes

* correct XML in getSpMetadataXML.php ([f5e9d2f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/f5e9d2fb0bb0816ea521a7474b36c05d0c5209fa))

## [9.7.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.7.1...v9.7.2) (2024-03-08)


### Bug Fixes

* correct typo in PerunCreateMember ([5ce7b46](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/5ce7b463c221bab7a52baffba2dace34ec377a2a))

## [9.7.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.7.0...v9.7.1) (2024-03-08)


### Bug Fixes

* update dependencies ([29f384f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/29f384fa9a46de37007f440a544028d1511e9d4e))

# [9.7.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.6.6...v9.7.0) (2024-02-12)


### Features

* theme for envri ([98fd64e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/98fd64e3c8e15ab38893149ea4a70b8b31a1dfea))

## [9.6.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.6.5...v9.6.6) (2024-02-09)


### Bug Fixes

* **deps:** allow authswitcher v12, update composer.lock ([0779a49](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/0779a49f491c5c04db4ba52ad6ac91414150b973))

## [9.6.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.6.4...v9.6.5) (2024-01-23)


### Bug Fixes

* correct Czech translation of MFA page ([3f9f64c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/3f9f64cf3ad8aa2d300f402c41e57c2f47788ce6))

## [9.6.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.6.3...v9.6.4) (2023-12-29)


### Bug Fixes

* 🐛 set upstream ACRs even if not requested by service ([e58b8b6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/e58b8b6af8e17e01ecf29da0f876972f12d6dc92))

## [9.6.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.6.2...v9.6.3) (2023-12-28)


### Bug Fixes

* 🐛 Fix SQL error for MySQL vs PGSQL in Challenges delete ([e30f642](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/e30f642491dc8988c44bf28615874764cec7af37))

## [9.6.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.6.1...v9.6.2) (2023-12-03)


### Bug Fixes

* 🐛 Fix possible errors in array_merge of Entitlements ([d72bf9b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/d72bf9b8b1b4e846ba07ad1483741796c4083afd))

## [9.6.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.6.0...v9.6.1) (2023-12-01)


### Bug Fixes

* filter by isSAML/isOIDC in list of SPs ([53483fa](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/53483fa866bb2df57543daefad7170e6a092dc43))
* skip non-SAML facilities in MetadataFromPerun ([7b94afd](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/7b94afdeaf6b4077d8b4f352c736d07b8161d3b5))

# [9.6.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.5.1...v9.6.0) (2023-10-04)


### Features

* method for getting resource attributes ([12d46ed](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/12d46edb87bba60d4951c38ea00b4071020f4fa9))

## [9.5.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.5.0...v9.5.1) (2023-09-25)


### Bug Fixes

* use the correct order in merging map attributes ([153ff60](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/153ff60e759845aebfc4dbea3e61409924a4fabc))

# [9.5.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.4.2...v9.5.0) (2023-09-13)


### Features

* 🎸 RPC method - find user by email ([5095ca4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/5095ca4aec08c6bd66641c7d90fcfdec58da355e))

## [9.4.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.4.1...v9.4.2) (2023-07-18)


### Bug Fixes

* hint for TOTP/recovery code field ([db466eb](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/db466ebb81a59070a24a784e9f88b3fca4af30ea))

## [9.4.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.4.0...v9.4.1) (2023-07-10)


### Bug Fixes

* use userId in ban filter ([e23dcb7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/e23dcb711801896088e2ad90d7a8549d2de514b7))

# [9.4.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.3.1...v9.4.0) (2023-06-01)


### Features

* allow update map  attr typ ([de2eb69](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/de2eb69e4b1a5327b41e327d52b678a133d0d4a8))

## [9.3.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.3.0...v9.3.1) (2023-05-24)


### Bug Fixes

* compatible with authswitcher v11 ([8d71a20](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/8d71a2021a4560b72ab4af0a31610ca0dcf4522f))

# [9.3.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.2.1...v9.3.0) (2023-05-24)


### Features

* filter IsBanned ([f489acf](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/f489acf92052ac150a6bf96dfb2b2d9049553529))

## [9.2.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.2.0...v9.2.1) (2023-04-12)


### Bug Fixes

* 🐛 Fix naming error preventing updateUes from working ([dc5005f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/dc5005f1ae6e461709f4cdd920fe15a729b3b42d))

# [9.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.1.0...v9.2.0) (2023-04-12)


### Features

* rename greylist to hide_from_discovery ([803054e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/803054e98b5b067d2f666b728d6af1d3f43b07fe))

# [9.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.0.8...v9.1.0) (2023-03-01)


### Bug Fixes

* improve wording of TOTP prompt ([42544f1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/42544f1860f6acd9ed13514f85a01aecc8e0b9cf))
* rpc method for get facilities by attr with attrs ([395f5e7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/395f5e7680d8ad6f476a5436da22d4ec98f16ed7))


### Features

* authProcFilter for creating member, new page for checking result, new AdapterRpc methods ([be97b40](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/be97b40ebac9e12c0771d0aef229eb7564832c59))
* getSpMetadataXML.php ([2611658](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/2611658cc8f7e79c38c3bb0f9f9a2e83b8d813a9))

## [9.0.8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.0.7...v9.0.8) (2022-12-15)


### Bug Fixes

* correct getFacilitiesByAttributeWithAttributes ([2c044b4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/2c044b4e4f00c22f86650e6872389e955cf37fba))

## [9.0.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.0.6...v9.0.7) (2022-12-15)


### Performance Improvements

* use getFacilitiesByAttributeWithAttributes ([5f0f625](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/5f0f625b33843f6749481c21755ec11d775e9cb8))

## [9.0.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.0.5...v9.0.6) (2022-12-12)


### Bug Fixes

* allow alphanumerical backup codes ([68c2410](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/68c24109fc74b6ad80371c0282e713bc49cfa796))

## [9.0.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.0.4...v9.0.5) (2022-11-29)


### Bug Fixes

* transform diacritics from data-search data in Disco.php ([0063bfe](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/0063bfe0bee351ffc92e2573fb218b52325cc642))

## [9.0.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/compare/v9.0.3...v9.0.4) (2022-11-03)


### Bug Fixes

* htmlspecialchars in AUP template ([cb35207](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/cb352075e47ef34514ee29a55b3c02b206d98550))
* increase RPC connector timeout ([ef5ab27](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perun/commit/ef5ab27544d43f62eb696670d4275b7cc49aa96a))

## [9.0.3](https://github.com/CESNET/perun-simplesamlphp-module/compare/v9.0.2...v9.0.3) (2022-08-11)


### Bug Fixes

* do not use object as array in getResourceCapabilities ([4075137](https://github.com/CESNET/perun-simplesamlphp-module/commit/40751372805a678901aa2c80436cbc5cafb7cf6c))

## [9.0.2](https://github.com/CESNET/perun-simplesamlphp-module/compare/v9.0.1...v9.0.2) (2022-07-22)


### Bug Fixes

* 🐛 Fix EntitlementUtils calling capabilities in LDAP ([e3aedd2](https://github.com/CESNET/perun-simplesamlphp-module/commit/e3aedd2a67242f6bea030247a4b5c6e04c3e23a1))

## [9.0.1](https://github.com/CESNET/perun-simplesamlphp-module/compare/v9.0.0...v9.0.1) (2022-07-22)


### Bug Fixes

* 🐛 Fix and refactor generating capabilities (resource and facility) ([009160a](https://github.com/CESNET/perun-simplesamlphp-module/commit/009160adf66c137d1c35b04a89abf29d90140f18))

# [9.0.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v8.2.1...v9.0.0) (2022-07-20)


### Bug Fixes

* use types array and dictionary instead of map ([d1d19f1](https://github.com/CESNET/perun-simplesamlphp-module/commit/d1d19f1fc9ac700f98dae6929a9bf49c8380be3c))


### chore

* remove deprecated mode values from ProxyFilter ([f2ec1fc](https://github.com/CESNET/perun-simplesamlphp-module/commit/f2ec1fc0430c6eb84aab8abe23061ed1b1bf53fa))


### Features

* use authswitcher in Disco to handle MFA ([244ae9e](https://github.com/CESNET/perun-simplesamlphp-module/commit/244ae9ee37a4f576790c14c77fde40c16cac65ae))


### BREAKING CHANGES

* replace "map" in attribute config with either "array" (ArrayList) or "dictionary" (LinkedHashMap)
* dropped filter MultifactorAcrs
* removed support for deprecated values blacklist and whitelist

## [8.2.1](https://github.com/CESNET/perun-simplesamlphp-module/compare/v8.2.0...v8.2.1) (2022-07-19)


### Bug Fixes

* typo in ProxyFilter ([e4036a9](https://github.com/CESNET/perun-simplesamlphp-module/commit/e4036a9acc70f1557ec24dfc90c6d2feb7190aa6))

# [8.2.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v8.1.1...v8.2.0) (2022-07-19)


### Features

* inclusive language in ProxyFilter ([b959c1d](https://github.com/CESNET/perun-simplesamlphp-module/commit/b959c1d29882ce1fce6f2c2cab341631401c2a7a))

## [8.1.1](https://github.com/CESNET/perun-simplesamlphp-module/compare/v8.1.0...v8.1.1) (2022-07-18)


### Bug Fixes

* 🐛 Fix fetching capabilities with no facility provided ([0469f41](https://github.com/CESNET/perun-simplesamlphp-module/commit/0469f4116a1cde10959417436630e2960552ae4c))

# [8.1.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v8.0.4...v8.1.0) (2022-07-08)


### Features

* 🎸 IsEligible authProc filter ([61dc7ce](https://github.com/CESNET/perun-simplesamlphp-module/commit/61dc7ce15862b72c50c0ff5fc680c38b4a409a7e))

## [8.0.4](https://github.com/CESNET/perun-simplesamlphp-module/compare/v8.0.3...v8.0.4) (2022-07-07)


### Bug Fixes

* add debug log to ForceAup ([413dac0](https://github.com/CESNET/perun-simplesamlphp-module/commit/413dac074f134b79457cebe3630c46abe64fd55b))

## [8.0.3](https://github.com/CESNET/perun-simplesamlphp-module/compare/v8.0.2...v8.0.3) (2022-06-20)


### Bug Fixes

* It is possible to rewrite default value for entityIdAttr ([1e8b21d](https://github.com/CESNET/perun-simplesamlphp-module/commit/1e8b21d36437ff4adc3eb205effaf22a172837d7))

## [8.0.2](https://github.com/CESNET/perun-simplesamlphp-module/compare/v8.0.1...v8.0.2) (2022-06-07)


### Bug Fixes

* input type number for OTP ([b1d7037](https://github.com/CESNET/perun-simplesamlphp-module/commit/b1d7037b6b47623a11cef3898628d17db930acd8))

## [8.0.1](https://github.com/CESNET/perun-simplesamlphp-module/compare/v8.0.0...v8.0.1) (2022-05-20)


### Bug Fixes

* **perunaup:** fix btn size and color ([bdaaa5a](https://github.com/CESNET/perun-simplesamlphp-module/commit/bdaaa5afb3cd4be853d3b8e6912115c0386c889d))

# [8.0.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.11.2...v8.0.0) (2022-05-19)


### Features

* new privacyIDEA template ([8bb43bc](https://github.com/CESNET/perun-simplesamlphp-module/commit/8bb43bc3f40358203f852480b62dd0d4dd047bcc))


### BREAKING CHANGES

* requires cesnet/simplesamlphp-module-privacyidea v5

## [7.11.2](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.11.1...v7.11.2) (2022-05-19)


### Bug Fixes

* 🐛 Fix templates perun-aup and perun-user, update dicts ([49d8ee0](https://github.com/CESNET/perun-simplesamlphp-module/commit/49d8ee0f9cdaf047b4c702d941129ee3ef3b8c69))

## [7.11.1](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.11.0...v7.11.1) (2022-05-18)


### Bug Fixes

* 🐛 Fix using approvalUrl where perunApprovalUrl should be u ([66e13ee](https://github.com/CESNET/perun-simplesamlphp-module/commit/66e13ee91da208c86690b67bd4a178909dc4f5b8))

# [7.11.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.10.1...v7.11.0) (2022-04-29)


### Features

* 🎸 Possibility to hide authN protocol, small fixes ([635ea64](https://github.com/CESNET/perun-simplesamlphp-module/commit/635ea641f5afa315dea6f1d11a504db769f33fc9))

## [7.10.1](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.10.0...v7.10.1) (2022-04-22)


### Bug Fixes

* 🐛 Fixed PrivacyIDEA template ([66b6656](https://github.com/CESNET/perun-simplesamlphp-module/commit/66b6656823ca2bc1c16349b1659c89574c7d7523))

# [7.10.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.9.0...v7.10.0) (2022-04-22)


### Features

* 🎸 Additional identifiers lookup ([36f7f7c](https://github.com/CESNET/perun-simplesamlphp-module/commit/36f7f7ceddeecb9ae532090f702f379b01c8c807))

# [7.9.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.8.2...v7.9.0) (2022-04-14)


### Features

* **forceaup:** new option entityID, fix required checks ([e2ec315](https://github.com/CESNET/perun-simplesamlphp-module/commit/e2ec3155db7d727c4c23c84471d3dc7ca68449c6))

## [7.8.2](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.8.1...v7.8.2) (2022-04-14)


### Bug Fixes

* Swaps getUsersGroupsOnSp and getUsersGroupsOnFacility methods ([660ba85](https://github.com/CESNET/perun-simplesamlphp-module/commit/660ba85369725e490ca1bf9b19757d748cbb61c4))

## [7.8.1](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.8.0...v7.8.1) (2022-04-13)


### Bug Fixes

* 🐛 Fix direct registration in SpAuthorization ([1e52a49](https://github.com/CESNET/perun-simplesamlphp-module/commit/1e52a49bbc2c0eccdbafb34d9b3ef0ac1751e33d))

# [7.8.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.7.0...v7.8.0) (2022-04-13)


### Bug Fixes

* Code checks ([fca9739](https://github.com/CESNET/perun-simplesamlphp-module/commit/fca9739d39ebb41d8cf38de257aacbc15a552c47))
* Minor fixes in AuthProc filters ([48c6949](https://github.com/CESNET/perun-simplesamlphp-module/commit/48c6949edc155aa722eb50bc87cd9136c8c0ee61))
* PerunEnsureMember sends users which are not in vo to regitration ([524c6ed](https://github.com/CESNET/perun-simplesamlphp-module/commit/524c6eddcb11c7c2cb4a210cb61d18eb01b15a72))
* Removes redundant log in updateUes script ([232d3b8](https://github.com/CESNET/perun-simplesamlphp-module/commit/232d3b82d03841e13c127a30e56cfa4d6b5b7b36))
* Rewrites aarc_idp_hint ([9657f72](https://github.com/CESNET/perun-simplesamlphp-module/commit/9657f72b5a2216ba240bc00382f992a22c211cbc))
* SpAuthorization - unouthorized when user is not in the request ([f201a15](https://github.com/CESNET/perun-simplesamlphp-module/commit/f201a15c135b6fccea77b611c7d453de443bde28))
* store a full attribute object from RPC ([efc0f8f](https://github.com/CESNET/perun-simplesamlphp-module/commit/efc0f8fce3ee77002f844191ad4ae64ea83484cb))
* Updates processFilterConfigurations-example ([760b6bd](https://github.com/CESNET/perun-simplesamlphp-module/commit/760b6bdc2905f122cce9a9a787dd1d3e9f321aba))
* updateUes - attr initialization from null to [] ([294f7c4](https://github.com/CESNET/perun-simplesamlphp-module/commit/294f7c4c8fa69f3b4fd804027ad1960847afdc9c))


### Features

* Adapter - getUsersGroupsOnSp, getGroupsWhereMemberIsActive ([18b6aed](https://github.com/CESNET/perun-simplesamlphp-module/commit/18b6aed3e0a1f523b54b97503c7ccb9391ba3fe2))
* PerunConstants ([520bbb7](https://github.com/CESNET/perun-simplesamlphp-module/commit/520bbb70cfde28ec930abdaf553f96be2300f0dd))
* PerunEnsureMember ([373d3a3](https://github.com/CESNET/perun-simplesamlphp-module/commit/373d3a3beda22bc9064c92f81498704d7dbeecc6))
* PerunUserGroups ([48fd82c](https://github.com/CESNET/perun-simplesamlphp-module/commit/48fd82c0c15faedc3390f4dbb969702c070bb019))
* SpAuthorization - adds handle_unsatisfied_membership option ([13ca45e](https://github.com/CESNET/perun-simplesamlphp-module/commit/13ca45e3faaf53ad089cd9db76c7d06c598745eb))
* UpdateUserExtSource - introduces appendOnlyAttrs, fixes the way how attrsToUpdate are created ([b241135](https://github.com/CESNET/perun-simplesamlphp-module/commit/b241135de6fbcc526213b7334f3480291850f358))

# [7.7.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.6.4...v7.7.0) (2022-04-11)


### Features

* ContactsToArray transformer ([015fb7f](https://github.com/CESNET/perun-simplesamlphp-module/commit/015fb7f8672cc6982e674406724d43f7e44c4ea2))

## [7.6.4](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.6.3...v7.6.4) (2022-04-06)


### Bug Fixes

* Filters ([96a75de](https://github.com/CESNET/perun-simplesamlphp-module/commit/96a75dea431e2cdd287462bd7dc76ac97e537f92))

## [7.6.3](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.6.2...v7.6.3) (2022-04-06)


### Bug Fixes

* 🐛 Fix reading configurati novalues in ExtractRequestAttrib ([60d2ffb](https://github.com/CESNET/perun-simplesamlphp-module/commit/60d2ffb01228208fa035267cbd8cbf94aa6839fa))
* 🐛 Small fix in redirects in the PerunUser filter ([e0166f6](https://github.com/CESNET/perun-simplesamlphp-module/commit/e0166f6e21e6a6c299d8da1f9e48d91bf169ee04))

## [7.6.2](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.6.1...v7.6.2) (2022-04-05)


### Bug Fixes

* 🐛 Fix JSON in perun dictionary ([41bf728](https://github.com/CESNET/perun-simplesamlphp-module/commit/41bf7281a5e1af734f0ad5edba260d988bfa4438))

## [7.6.1](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.6.0...v7.6.1) (2022-04-04)


### Bug Fixes

* 🐛 Fix default value in ForceAup due to strictypes ([eb75544](https://github.com/CESNET/perun-simplesamlphp-module/commit/eb75544958bf6feee5bbd644b7aa7b872a21402b))

# [7.6.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.5.1...v7.6.0) (2022-04-04)


### Bug Fixes

* 🐛 Fix code style ([77729ea](https://github.com/CESNET/perun-simplesamlphp-module/commit/77729ea0fc11eb37f7c2576d2866bfb79f372b62))


### Features

* 🎸 AuthProcFilter GenerateIdPAttributes ([a2ca6ea](https://github.com/CESNET/perun-simplesamlphp-module/commit/a2ca6ea455bbcd281d3d9fefb564e39c32f5e7e0))
* 🎸 AuthProcFilter PerunUser - identify user from Perun ([b31976a](https://github.com/CESNET/perun-simplesamlphp-module/commit/b31976a4ace2fd0d592632e83131d453e3a6b103))
* 🎸 AuthProcFilter QualifyNameID ([1f8bd75](https://github.com/CESNET/perun-simplesamlphp-module/commit/1f8bd750bb6013d56ea4332c50b38560aa955fcd))
* 🎸 DropUserAttributes authProcFilter ([c763ad9](https://github.com/CESNET/perun-simplesamlphp-module/commit/c763ad9e97cc2fd6b191f7b95f096ce6a042255f))
* 🎸 New filter for extracting attribute from request var ([6c6110f](https://github.com/CESNET/perun-simplesamlphp-module/commit/6c6110fd0ab2eee62ee68a0e819fbb94bf2b185e))
* 🎸 PerunAup authProcFilter ([301139a](https://github.com/CESNET/perun-simplesamlphp-module/commit/301139a4c72357e65dfa1b5f2423c179fb080d75))
* 🎸 SpAuthorization authproc filter ([5771a1b](https://github.com/CESNET/perun-simplesamlphp-module/commit/5771a1b3cf04db4805f26cee3a0934ddb2399fe1))
* Consolidator app ([e7bbde9](https://github.com/CESNET/perun-simplesamlphp-module/commit/e7bbde9a85f8c0d67a16e6987a7614bbc9bb4995))

## [7.5.1](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.5.0...v7.5.1) (2022-04-01)


### Bug Fixes

* getPerunUser name construction ([ec7150a](https://github.com/CESNET/perun-simplesamlphp-module/commit/ec7150a8cd75b40f57e808cde1108fd7425c249e))

# [7.5.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.4.0...v7.5.0) (2022-03-30)


### Features

* updateUes - configurable identifiers ([2a3d052](https://github.com/CESNET/perun-simplesamlphp-module/commit/2a3d052b65e49ceb1f6128f29ed58465d2d3da8d))

# [7.4.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.3.0...v7.4.0) (2022-03-29)


### Features

* Do not show previous selection for SPs listed in config ([dda8140](https://github.com/CESNET/perun-simplesamlphp-module/commit/dda81406b58f92eb472fd6226adcde2ef6612349))

# [7.3.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.2.1...v7.3.0) (2022-03-18)


### Features

* Custom AttributeMap filter ([903bd6f](https://github.com/CESNET/perun-simplesamlphp-module/commit/903bd6fc3f6a349c2ed359bc34edcd6f3cf72858))

## [7.2.1](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.2.0...v7.2.1) (2022-03-11)


### Bug Fixes

* 🐛 Fix privacyIDEA form for new version of PI module ([9a67d39](https://github.com/CESNET/perun-simplesamlphp-module/commit/9a67d39cf3e9e81077b3b62630434bde296f8db3))

# [7.2.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.1.1...v7.2.0) (2022-03-09)


### Features

* Custom privacyIDEA login template ([15359e0](https://github.com/CESNET/perun-simplesamlphp-module/commit/15359e0d03b60d6ffbba76f5e0dc799785151ac2))

## [7.1.1](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.1.0...v7.1.1) (2022-03-07)


### Bug Fixes

* Fixed AUP filter ([9ecf4c0](https://github.com/CESNET/perun-simplesamlphp-module/commit/9ecf4c01eb2ec3e823b69f16ffadd389b4760f20))

# [7.1.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.0.3...v7.1.0) (2022-01-13)


### Features

* 🎸 Added RestoreAcrs authproc filter, modify ACRs when MFA ([ebafb05](https://github.com/CESNET/perun-simplesamlphp-module/commit/ebafb059323b8808fb0c04009e61d9ab5fa123b5))

## [7.0.3](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.0.2...v7.0.3) (2022-01-11)


### Bug Fixes

* refactor disco ([#218](https://github.com/CESNET/perun-simplesamlphp-module/issues/218)) ([31f8216](https://github.com/CESNET/perun-simplesamlphp-module/commit/31f82168cf0f294c43490e7a79e102542e582530))

## [7.0.2](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.0.1...v7.0.2) (2022-01-11)


### Bug Fixes

* **forceaup:** drop unused option uidAttr ([#215](https://github.com/CESNET/perun-simplesamlphp-module/issues/215)) ([a18fad6](https://github.com/CESNET/perun-simplesamlphp-module/commit/a18fad6da7ea41a154eb4b497d5f5a7a503c9cad)), closes [#157](https://github.com/CESNET/perun-simplesamlphp-module/issues/157)

## [7.0.1](https://github.com/CESNET/perun-simplesamlphp-module/compare/v7.0.0...v7.0.1) (2022-01-05)


### Bug Fixes

* Fixed some unchecked potential errors ([#204](https://github.com/CESNET/perun-simplesamlphp-module/issues/204)) ([617153c](https://github.com/CESNET/perun-simplesamlphp-module/commit/617153c41cae8c187ffa2c529d76c5966bfe25c6))

# [7.0.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.4.3...v7.0.0) (2022-01-05)


### chore

* add missing dependencies, PHP >= 7.1, SSP 1.19, add package-lock ([6c873af](https://github.com/CESNET/perun-simplesamlphp-module/commit/6c873af0533c20acfde90399bf5836ef67a9a299))


### BREAKING CHANGES

* PHP 7.1 or higher is required, SSP 1.19 is required

## [6.4.3](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.4.2...v6.4.3) (2021-12-13)


### Bug Fixes

* typo in RpcConnector ([4e15e8b](https://github.com/CESNET/perun-simplesamlphp-module/commit/4e15e8ba350db256b39417a307e0db3a8a8c1c13))

## [6.4.2](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.4.1...v6.4.2) (2021-11-25)


### Bug Fixes

* make database required for challenges, skip challenge cleanup without database ([c42c3fa](https://github.com/CESNET/perun-simplesamlphp-module/commit/c42c3fa2a3be98509d0d3e9037fff2a0722a5db1)), closes [#182](https://github.com/CESNET/perun-simplesamlphp-module/issues/182)

## [6.4.1](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.4.0...v6.4.1) (2021-11-24)


### Bug Fixes

* 🐛 Fix wrong variable names in getFacilityByXY methods ([986a7d8](https://github.com/CESNET/perun-simplesamlphp-module/commit/986a7d85de7fbfd3e63f4a7587bb80e473252376))

# [6.4.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.3.3...v6.4.0) (2021-11-24)


### Features

* Added possibility to add a service name on WAYF ([1c84441](https://github.com/CESNET/perun-simplesamlphp-module/commit/1c844417dd241ac2255713387b2b2052760a70e8))

## [6.3.3](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.3.2...v6.3.3) (2021-11-16)


### Bug Fixes

* 🐛 Remove fixed footer for warning_test_sp ([540afac](https://github.com/CESNET/perun-simplesamlphp-module/commit/540afac23a60916d1676998bb96ba18b5720676d))

## [6.3.2](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.3.1...v6.3.2) (2021-11-15)


### Bug Fixes

* prevent type errors in RPC connector ([5152cbe](https://github.com/CESNET/perun-simplesamlphp-module/commit/5152cbe154f950c1a83b25c7e8c6e8d68051bf12))

## [6.3.1](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.3.0...v6.3.1) (2021-11-03)


### Bug Fixes

* 🐛 Added missing ext-intl to the composer.json ([e79bd2a](https://github.com/CESNET/perun-simplesamlphp-module/commit/e79bd2a59547e4ccfd674a4f962fad285c17c2ac))

# [6.3.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.2.0...v6.3.0) (2021-10-12)


### Bug Fixes

* 🐛 Add check of key existence in template - unauth-acc-reg ([34c10d5](https://github.com/CESNET/perun-simplesamlphp-module/commit/34c10d52df8afcac2f07c4ee4d3fb13401c5f8bc))


### Features

* Turn off addInstitution when whitelisting is disabled ([91990b5](https://github.com/CESNET/perun-simplesamlphp-module/commit/91990b55d94766cff330a6e686bb5b4fe7ec45a2))

# [6.2.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.1.1...v6.2.0) (2021-10-12)


### Features

* Added support for old browsers ([4d62561](https://github.com/CESNET/perun-simplesamlphp-module/commit/4d62561cf8ca7ba2bb3645027541f0bc11c34681))

## [6.1.1](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.1.0...v6.1.1) (2021-09-29)


### Bug Fixes

* Changed text labels on consent ([1764572](https://github.com/CESNET/perun-simplesamlphp-module/commit/1764572365e015291c35ac7ae03679bd9000d52e))

# [6.1.0](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.0.7...v6.1.0) (2021-09-21)


### Bug Fixes

* Fixed ECS bugs ([0ac5a9f](https://github.com/CESNET/perun-simplesamlphp-module/commit/0ac5a9f910c1496d46d4e398ece4306b03bce868))


### Features

* Added metadata expiration page ([e1ad062](https://github.com/CESNET/perun-simplesamlphp-module/commit/e1ad062c7747be6ff2fb68ccbd8e01d43318a904))

## [6.0.7](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.0.6...v6.0.7) (2021-09-10)


### Bug Fixes

* bugfixes in list of SPs ([1cd84a8](https://github.com/CESNET/perun-simplesamlphp-module/commit/1cd84a8bdbe879bdec568b1952b8756f29d99f94))

## [6.0.6](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.0.5...v6.0.6) (2021-08-19)


### Bug Fixes

* fix bad import of Exceptions ([bdd51b4](https://github.com/CESNET/perun-simplesamlphp-module/commit/bdd51b46edb765270d282c302176aaf3e01b9117))

## [6.0.5](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.0.4...v6.0.5) (2021-08-18)


### Bug Fixes

* 🐛 fix not checking for key existence in aups ([00cf0f0](https://github.com/CESNET/perun-simplesamlphp-module/commit/00cf0f0b53b1f4828cf12208976124a06a72bafa))
* 🐛 refactored AUPs DateTime treatment in ForceAup ([5130dfc](https://github.com/CESNET/perun-simplesamlphp-module/commit/5130dfc37b4f6bb847ee09d0f7ae57ffb8809477))

## [6.0.4](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.0.3...v6.0.4) (2021-08-18)


### Bug Fixes

* fix bad return type in DatabaseCommand ([95328ba](https://github.com/CESNET/perun-simplesamlphp-module/commit/95328bab777f595683342aa77141a3b1e6bb7d10))

## [6.0.3](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.0.2...v6.0.3) (2021-08-18)


### Bug Fixes

* fix duplicate lines in challenges ([360db1a](https://github.com/CESNET/perun-simplesamlphp-module/commit/360db1adf6b9bd1d00e0edc72ebaf2ee41961dc4))

## [6.0.2](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.0.1...v6.0.2) (2021-08-18)


### Bug Fixes

* Refactor ForcAup filter ([7ef157e](https://github.com/CESNET/perun-simplesamlphp-module/commit/7ef157e6023db9afdfde455e4331970ce9c2f9a8))

## [6.0.1](https://github.com/CESNET/perun-simplesamlphp-module/compare/v6.0.0...v6.0.1) (2021-08-10)


### Bug Fixes

* fix processing attr val  of map type in LDAP ([d892ca9](https://github.com/CESNET/perun-simplesamlphp-module/commit/d892ca944d92b0de2e821d4a4421cce84bc5b514))

## [v6.0.0]
#### Changed
- Improve WAYF searching by localized name and domain
- Implemented filter EnsureVoMember
- Security improvements in script calls

#### Fixed
- Detailed endpoint format when spaced in EndpointMapToArray 
- Revert change to INDEX_MIN in EndpointMapToArray
- Rename the hook correctly to naming convention
- Each log has just one line output

## [v5.2.0]
#### Added
- Added possibility to use a callable for entityID parameter in PerunEntitlement(Extended)

## [v5.1.1]
#### Fixed
- Fixed removal of filtered authnContextClassRefs in disco
 
## [v5.1.0]
#### Added
- Added possibility to add custom texts to the TEST_SP warning page.

#### Changed
- Use translation  for privacy policy document block on consent screen from module Perun
- Connection to the database obtained through the SimpleSAML Database class

#### Fixed
- Fixed bad check in NagiosStatusConnector.php

## [v5.0.0]
#### Added
- Added extended PerunEntitlements

#### Changed
- Refactored Disco page. See the config template for example configuration.
- Obtaining the data from Nagios is done through SSH instead of a certificate and calling an API

#### Fixed
- Fixed bug in PerunAttributes.php for PARTIAL mode when mapping one Perun attribute to more internal attributes 
  caused getting attributes from Perun every time.

## [v4.1.1]
#### Fixed
- Fixed bad log message in PerunIdentity in mode USERONLY

## [v4.1.0]
#### Changed
- Allow using Perun RPC serializer from the configuration. Default value is 'json'. 
- Add new option 'mode' for PerunIdentity process filter:
    - mode: 'FULL' - Get the user from Perun and check if user has correct rights to access service
    - mode: 'USERONLY' - Get the user from Perun only

## [v4.0.4]
#### Fixed
- Fixed getting SP name from 'UIInfo>DisplayName'

## [v4.0.3]
#### Fixed
- Fixed works with internal attr name in MetadataToPerun/MetadataFromPerun

## [v4.0.2]
#### Fixed
- Fixed getting attributes from Perun in partial mode
    - Allow to store one source attribute to more destination attributes

## [v4.0.1]
#### Fixed
- Fixed getting attributes in class ForceAup

## [v4.0.0]
#### Added
- Added some methods for getting values to Adapter.php
- Added fallback to RPC for methods we're not able to run in LDAP
- Add getFacilityAdmins method to RPC Connector

#### Changed
- Changed the way of getting attribute names for interfaces: through internal attribute names in perun_attributes.php config
- Return sorted eduPersonEntitlement
- Don't show previous selection when user show all entries on the discovery page
- ListOfSps 
    - Don't show the description by default
    - Added required attribute 'listOfSps.serviceNameAttr' !!!
    - Add translation for multi-languages attributes

#### Fixed
- Fixed Updating UES in Perun

## [v3.9.0]
#### Added
- Added facility capabilities to PerunEntitlement
- Added process filter for logging info about login

#### Changed
- Use object `Configuration` for getting base module configuration
- Add possibility to select mode(whitelist/blacklist) in ProxyFilter.php
    * The default option is blacklist
- Allow call multiple ProcessFilter in one ProxyFilter module

#### Fixed
- Fixed the width of showed tagged idps in case the count of idps is equal to (x * 3) + 1
- Using try{}catch{} to avoid to PerunException in PerunEntitlement.php
- Return [] instead of null in getFacilityCapability via RPC, if facilityCapability is not set

## [v3.8.0]
#### Changed
- Releasing forwardedEduPersonEntitlement is now optional (forwardedEduPersonEntitlement are released by default)

#### Fixed
- Fixed problem with getting group without description from LDAP 
    * Before: Exeption
    * Now: Description is ''
- Fixed releasing entitlement for Virtual Organization
    * Before: einfra:members
    * Now: einfra

#### Removed
- Removed deprecated getFacilitiesByEntityId method

## [v3.7.4]
#### Added
- Added logging response time for each request into RPC/LDAP

#### Changed
- If needed to get more facility attributes, method getFacilityAttributesValues() is used instead of several calls of getFacilityAttribute()

#### Fixed
- Fix logging request params

## [v3.7.3]
#### Fixed
- Fixed the bug from [bc3ec33] which caused that the updating UES didn't work.
- Use the same prefix for all messages in updateUes.php

[bc3ec33]:https://github.com/CESNET/perun-simplesamlphp-module/commit/bc3ec33c8f5088f7be712b8e5a0e70f229731648

## [v3.7.2]
#### Fixed
- Allow omitted config for nested class in ProxyFilter
- Fixed bad call of function 'showTaggedEntry()'

## [v3.7.1]
#### Fixed
- Using correct const for EntitlementPrefix in PerunEntitlement.php
- Added missing 'group' between entitlementPrefix and groupName in mapGroupName()

## [v3.7.0]
#### Changed
- UserExtSources are now updated asynchronously

#### Fixed
- Fix method stringify in StringifyTargetedID.php to be compatible with SimpleSAMLphp 1.18.0+
    - Using getters to get private properties

## [v3.6.0]
#### Added
- Added method getFacilityByEntityId
- Added resource capabilities into entitlements

#### Changed
- Slightly modified text displayed on WAYF
- Updated phpcs ruleset to PSR-12
- is_null() changed to === null
- Using identity comparison instead of equality comparison
- Removed checks in ifs that var is (not) null before empty(var) function (empty checks that itself)
- Double quotes changed to single quotes
- getFacilitiesByEntityId marked as deprecated (getFacilityByEntityId should be used instead)
- Using of getFacilityByEntityId instead of getFacilitiesByEntityId
- Filters JoinGroupsAdnEduPersonEntitlement and PerunGroups merged into PerunEntitlement
- Using expression in asserts (String in assert() is DEPRECATED from PHP 7.2)

#### Fixed
- Fixed wrong dictionary name in post.php
- Removed unnecessary include
- Resolve problem with Sideeffects (PSR1.Files.SideEffects)

## [v3.5.2]
#### Fixed
- Fixed the header on consentform

## [v3.5.1]
#### Fixed
- Fixed bug in filtering IdPs on DS

## [v3.5.0]
#### Changed
- Updated consent page
    - Consent page is shown as a list instead of a teble
    - Changes in dictionary
    - Change the width for keys(col-sm-5) and values(col-sm-7)
- Added filterAttributes option to ProxyFilter for filtering out based on user attribute values

## [v3.4.1]
#### Fixed
- Fixed bugs in disco-tpl.php

## [v3.4.0]
#### Changed
- Remove star which was shown on items on Discovery Service. Now the star will be shown only at previously selected IdP.
- Change work with IdP entities with tags 'social' and  'preferred' on DS
    - Width of entities is now counted automatically
    - Social IdP has 'Sign in with' before name, Preferred IdP hasn't
    - Added possibility to change display name in attribute 'fullDisplayName' in metadata
- If user's last selected IdP is known then show only this IdP and button to show all IdPs 
- Set autofocus on previously selected IdP if exist
- Removed unused function showIcon() in disco-tpl.php

#### Fixed
- Fixed the bug in 'getEntitylesAttribute' function to return correct value of Entityless attribute 
- Fixed the bug in getting new aups to sign

## [v3.3.0]
#### Added
- Added endpoint to get filtered list of metadata in format:
```json
[
  {
    "entityid": "https://entityid1/",
    "name": {
      "en": "IdP1",
      "cs": "IdP1"
    }
  },
  { ... }
]
```
- Added warning types: INFO, WARNING, ERROR

#### Changed
- RpcConnector now stores cookie into file
- Set CONNECTTIMEOUT and TIMEOUT in RpcConnector
- Use new object perunFacility in LDAP to search information about facility
- Configuration for warning on DS is now in module_perun.php

## [v3.2.1]
#### Fixed
- Fixed bug in redirect to registration in case only one VO and one group is available


## [v3.2.0]
#### Added
- Added filter JoinGroupsAndEduPersonEntitlement

#### Changed
- Using of short array syntax (from array() to [])
- Added modes into PerunAttribute process filter
    - MODE_FULL - Rewrite all attributes specified in config
    - MODE_PARTIAL - Rewrite only unset attributes
- Chart.bundle.js is now loaded from SSP module instead of directly from internet

#### Fixed
- Fixed the problem that IDP filter on WAYF didn't work correctly
- Fixed bad error message when the process of bind user to LDAP failed
- Fixed style errors

## [v3.1.1]
#### Fixed
- Added checks into UpdateUserExtSource process filter to prevent undefined index or undefined offset errors 

## [v3.1.0]
#### Added
- PerunAttribute process filter - Added support for numeric attributes

## [v3.0.4]
#### Fixed
- Added missing space before 'addInstitutionButton' or link
- Added missing import
- Fixed the style of changelog
- Fixed the checks in method getMemberStatusByUserAndVo() in AdapterLDAP

## [v3.0.3]
#### Fixed
- Use ldap base from variable in AdapterLdap::getMemberStatusByUserAndVo() instead of static string

## [v3.0.2]
#### Fixed
- Fixed error in case of call method getIdps() with unused tag 

## [v3.0.1]
#### Fixed
- Fixed showing entry on wayf with tag 'preferred'

## [v3.0.0]
#### Added
- Added file phpcs.xml
- Added basic versions of template files

#### Changed
- Changed code standard to PSR-2
- Module uses namespaces
- Changed name of the classes below:
    - sspmod_perun_Auth_Process_ForceAup to SimpleSAML\Module\perun\Auth\Process\ForceAup
    - sspmod_perun_Auth_Process_IdPAttribute to SimpleSAML\Module\perun\Auth\Process\IdpAttribute
    - sspmod_perun_Auth_Process_PerunAttributes to SimpleSAML\Module\perun\Auth\Process\PerunAttributes
    - sspmod_perun_Auth_Process_PerunGroups to SimpleSAML\Module\perun\Auth\Process\PerunGroups
    - sspmod_perun_Auth_Process_PerunIdentity to SimpleSAML\Module\perun\Auth\Process\PerunIdentity
    - sspmod_perun_Auth_Process_ProcessTargetedID to SimpleSAML\Module\perun\Auth\Process\ProcessTargetedID
    - sspmod_perun_Auth_Process_ProxyFilter to SimpleSAML\Module\perun\Auth\Process\ProxyFilter
    - sspmod_perun_Auth_Process_RemoveAllAttributes to SimpleSAML\Module\perun\Auth\Process\RemoveAllAttributes
    - sspmod_perun_Auth_Process_RetainIdPEntityID to SimpleSAML\Module\perun\Auth\Process\RetainIdPEntityID
    - sspmod_perun_Auth_Process_StringifyTargetedID to SimpleSAML\Module\perun\Auth\Process\StringifyTargetedID
    - sspmod_perun_Auth_Process_UpdateUserExtSource to SimpleSAML\Module\perun\Auth\Process\UpdateUserExtSource
    - sspmod_perun_Auth_Process_WarningTestSP to SimpleSAML\Module\perun\Auth\Process\WarningTestSP
    - sspmod_perun_model_Facility to SimpleSAML\Module\perun\model\Facility
    - sspmod_perun_model_Group to SimpleSAML\Module\perun\model\Group
    - sspmod_perun_model_HasId to SimpleSAML\Module\perun\model\HasId
    - sspmod_perun_model_Member to SimpleSAML\Module\perun\model\Member
    - sspmod_perun_model_Resource to SimpleSAML\Module\perun\model\Resource
    - sspmod_perun_model_User to SimpleSAML\Module\perun\model\User
    - sspmod_perun_model_Vo to SimpleSAML\Module\perun\model\Vo
    - sspmod_perun_Adapter to SimpleSAML\Module\perun\Adapter
    - sspmod_perun_AdapterLdap to SimpleSAML\Module\perun\AdapterLdap
    - sspmod_perun_AdapterRpc to SimpleSAML\Module\perun\AdapterRpc
    - DatabaseCommand to SimpleSAML\Module\perun\DatabaseCommand
    - DatabaseConnector to SimpleSAML\Module\perun\DatabaseConnector
    - sspmod_perun_Disco to SimpleSAML\Module\perun\Disco
    - sspmod_perun_DiscoTemplate to SimpleSAML\Module\perun\DiscoTemplate
    - sspmod_perun_Exception to SimpleSAML\Module\perun\Exception
    - sspmod_perun_IdpListsService to SimpleSAML\Module\perun\IdpListsService
    - sspmod_perun_IdpListsServiceCsv to SimpleSAML\Module\perun\IdpListsServiceCsv
    - sspmod_perun_IdpListsServiceDB to SimpleSAML\Module\perun\IdpListsServiceDB
    - sspmod_perun_LdapConnector to SimpleSAML\Module\perun\LdapConnector
    - sspmod_perun_RpcConnector to SimpleSAML\Module\perun\RpcConnector
- Added disco-tpl template file
- Method getUsersGroupsOnFacility in AdapterRpc was optimized
- Searching of institutions on WAYF is accent-insensitive
- Changed config file for listOfSps

#### Fixed
- Fixed the bug generating Array to string conversion Exception in PerunAttributes, 
when storing one Perun attribute to more SAML attribute 

#### Removed
- Removed template config file module_perun_listOfSps.php 
(Configuration of listOfSps.php page is moved to module_perun.php)

## [v2.2.0]

#### Added
- List of services is displayed as JSON if parameter 'output=json' is set in URL
- Page showing status of selected components
   - This page is also available in JSON format if parameter 'output=json' is set in URL

#### Changed
- Updated composer.json dependencies

#### Fixed
- Fixed the problem where LDAP calls RPC method in PerunIdentity filter
- Fixed assignation of one Perun attribute to multiple SP attributes

## [v2.1.0]
#### Added
- Added new atribute in PerunIdentity process filter with list of Services identifier's for which we don't want to show page with information, that the user will be redirected to other page 

#### Changed
- Changed design of ListOfSps
- Changed the texts and visual form of pages: perun_identity_choose_vo_and_group.php and unauthorized_access_go_to_registration.php

#### Fixed
- Fixed resend SPMetadata from request to unauthorized-access-go-to-registration page
- Fixed url encoding in PerunGroups

## [v2.0.0]
#### Added
- Added badges to README
- Added page with configurable table of SPs on Proxy
- Added new model Member
- Added new model Resource
- New methods for getting data from Perun LDAP and Perun RPC
- Added function for generating metadata for SimpleSAMLphp Proxy AAI from Perun
- Added UpdateUserExtSource filter

#### Changed
- Connectors methods are not static for now.
- Added constructors to Adapters, which allows specified config file for each connections.
- New properties voId and uniqueName in Group model
- Function getSpGroup require only one param($spEntityId)
- Function unauthorize in PerunIdentity is now public
- Changed the login and registration process

#### Fixed
- Fixed the problem with access to non-secured LDAP
- Fixed the bad call of function 'searchForEntity(...)' in function getVoById() in AdapterLdap.php  

## [v1.0.0]

[Unreleased]: https://github.com/CESNET/perun-simplesamlphp-module/tree/master
[v6.0.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v6.0.0
[v5.2.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v5.2.0
[v5.1.1]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v5.1.1
[v5.1.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v5.1.0
[v5.0.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v5.0.0
[v4.1.1]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v4.1.1
[v4.1.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v4.1.0
[v4.0.4]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v4.0.4
[v4.0.3]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v4.0.3
[v4.0.2]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v4.0.2
[v4.0.1]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v4.0.1
[v4.0.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v4.0.0
[v3.9.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.9.0
[v3.8.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.8.0
[v3.7.4]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.7.4
[v3.7.3]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.7.3
[v3.7.2]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.7.2
[v3.7.1]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.7.1
[v3.7.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.7.0
[v3.6.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.6.0
[v3.5.2]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.5.2
[v3.5.1]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.5.1
[v3.5.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.5.0
[v3.4.1]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.4.1
[v3.4.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.4.0
[v3.3.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.3.0
[v3.2.1]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.2.1
[v3.2.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.2.0
[v3.1.1]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.1.1
[v3.1.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.1.0
[v3.0.4]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.0.4
[v3.0.3]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.0.3
[v3.0.2]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.0.2
[v3.0.1]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.0.1
[v3.0.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v3.0.0
[v2.2.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v2.2.0
[v2.1.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v2.1.0
[v2.0.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v2.0.0
[v1.0.0]: https://github.com/CESNET/perun-simplesamlphp-module/tree/v1.0.0
